package dai.samples.rollback;

import dai.samples.batch.BatchApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 * @author daify
 * @date 2020-11-15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class RollBackJobConfigTests {


    @Autowired
    @Qualifier(value = "rollBackJob")
    private Job rollBackJob;

    @Autowired
    @Qualifier(value = "rollBackErrorJob")
    private Job rollBackErrorJob;

    @Autowired
    private JobLauncher jobLauncher;

    /**
     * 测试不进行回滚的操作，正常每一个chunk中有数据异常整个chunk会被回滚，
     * 而此操作不会进行回滚，可以尝试修改chunk的参数看不同的效果
     * @throws Exception
     */
    @Test
    public void testRollBackJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(rollBackJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        //JobExecution jobExecution = jobLauncher.run(rollBackJob, new JobParameters());
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * 测试不进行回滚的操作，正常每一个chunk中有数据异常整个chunk会被回滚，
     * 而此操作不会进行回滚，可以尝试修改chunk的参数看不同的效果
     * @throws Exception
     */
    @Test
    public void testRollBackErrorJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(rollBackErrorJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        //JobExecution jobExecution = jobLauncher.run(rollBackJob, new JobParameters());
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }
}
