package dai.samples.add;

import dai.samples.batch.BatchApplication;
import dai.samples.batch.config.BatchConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Arrays;

/**
 * 添加子任务的测试代码
 * @author daify
 * @date 2020-11-16
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class AddJobTests {


    @Autowired
    @Qualifier(value = "addJob")
    private Job addJob;



    @Autowired
    private JobLauncher jobLauncher;


    /**
     * 添加子任务的测试流程
     * @throws Exception
     */
    @Test
    public void testAddJob() throws Exception {

        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobParameters jobParameters = new JobParametersBuilder().addString("type", "FAILED").toJobParameters();
        JobExecution jobExecution = jobLauncher.run(addJob, jobParameters);
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
        File[] newFiles = file.listFiles();
        Arrays.stream(newFiles).forEach(item -> System.out.println(item.getName()));
    }

}
