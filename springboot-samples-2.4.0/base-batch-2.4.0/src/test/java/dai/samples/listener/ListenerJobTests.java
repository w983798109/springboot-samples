package dai.samples.listener;

import dai.samples.batch.BatchApplication;
import dai.samples.batch.config.BatchConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Arrays;

/**
 * 监听器的测试
 * @author daify
 * @date 2020-11-15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class ListenerJobTests {


    @Autowired
    @Qualifier(value = "listenerJob")
    private Job listenerJob;

    @Autowired
    private JobLauncher jobLauncher;

    /**
     * 这个测试方法可以测试监听器的相关内容
     * 测试不进行回滚的操作，正常每一个chunk中有数据异常整个chunk会被回滚，
     * 而此操作不会进行回滚，可以尝试修改chunk的参数看不同的效果
     * @throws Exception
     */
    @Test
    public void testListenerJob() throws Exception {

        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobExecution jobExecution = jobLauncher.run(listenerJob, new JobParameters());
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
        File[] newFiles = file.listFiles();
        Arrays.stream(newFiles).forEach(item -> System.out.println(item.getName()));
    }
}
