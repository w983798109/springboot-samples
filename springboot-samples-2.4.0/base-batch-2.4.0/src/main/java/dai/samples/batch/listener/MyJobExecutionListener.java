package dai.samples.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * @author daify
 * @date 2020-11-15
 */
@Slf4j
public class MyJobExecutionListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("-------------beforeJob--------------");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("-------------afterJob--------------");
    }
}
