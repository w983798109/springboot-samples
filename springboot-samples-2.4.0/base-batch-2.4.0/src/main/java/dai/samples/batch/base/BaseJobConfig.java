package dai.samples.batch.base;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * 基础任务配置
 * @author daify
 * @date 2020-11-03
 */
@Slf4j
@Component
public class BaseJobConfig {

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    PlatformTransactionManager transactionManager;

    /**
     * 任务
     * @param jobRepository
     * @return
     */
    @Bean("sampleJob")
    public Job sampleJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("sampleJob")
                .repository(jobRepository)
                .start(sampleStep())
                .build();
    }

    /**
     * 步骤处理
     * @return
     */
    @Bean(value = "sampleStep")
    public Step sampleStep() {
        return this.stepBuilderFactory.get("sampleStep")
                .transactionManager(transactionManager)
                .<BatchEntity, BatchEntity>chunk(1)
                .reader(itemReader())
                .processor(getProcessor())
                .writer(itemWriter())
                .build();
    }


    /**
     * JSON写数据
     * 在target/test-outputs目录下写相关结果
     * @return
     */
    public ItemWriter<BatchEntity> itemWriter() {
        long time = System.currentTimeMillis();
        String name = "sampleJob-" + time;
        String patch = "target/test-outputs/" + name + ".json";
        return new JsonFileItemWriterBuilder<BatchEntity>()
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(patch))
                .name("batchJobWriter")
                .build();
    }

    /**
     * JSON读数据
     * 读取resource/data下的JSON文件
     * @return
     */
    public ItemReader<BatchEntity> itemReader() {
        return new JsonItemReaderBuilder<BatchEntity>()
                .jsonObjectReader(new JacksonJsonObjectReader<>(BatchEntity.class))
                .resource(new ClassPathResource("data/batchJob.json"))
                .name("batchJobReader")
                .build();
    }

    /**
     * 新建任务处理器
     * @return
     */
    public ItemProcessor<? super BatchEntity, ? extends BatchEntity> getProcessor() {
        return new ChangeNameProcessor();
    }


    /**
     * 任务处理器
     */
    class ChangeNameProcessor implements ItemProcessor<BatchEntity, BatchEntity> {

        @Override
        public BatchEntity process(BatchEntity person) {
            String fullName = "sampleJob-" + person.getFirstName() + " " +
                    person.getLastName();
            person.setFullName(fullName);
            log.info(fullName);
            return person;
        }
    }

}
