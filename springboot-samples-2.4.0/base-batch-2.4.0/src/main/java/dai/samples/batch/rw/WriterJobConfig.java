package dai.samples.batch.rw;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * 写数据的任务
 * @author daify
 * @date 2020-12-27
 */
@Slf4j
@Component
public class WriterJobConfig {

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    /**
     * JSON读取
     */
    @Autowired
    @Qualifier(value = "itemJsonReader")
    ItemReader<BatchEntity> itemJsonReader;


    /**
     * JSON写数据
     */
    @Autowired
    @Qualifier(value = "itemJsonWriter")
    ItemWriter<BatchEntity> itemJsonWriter;

    /**
     * 写入文件中行形式
     */
    @Autowired
    @Qualifier(value = "itemLineWriter")
    FlatFileItemWriter<BatchEntity> itemLineWriter;

    /**
     * XML写数据
     */
    @Autowired
    @Qualifier(value = "itemXmlWriter")
    StaxEventItemWriter<BatchEntity> itemXmlWriter;

    /**
     * 数据写到数据库中
     */
    @Autowired
    @Qualifier(value = "itemJDBCWriter")
    JdbcBatchItemWriter<BatchEntity> itemJDBCWriter;


    /**
     * JSON写数据
     * @param jobRepository
     * @return
     */
    @Bean("itemJsonWriterJob")
    public Job itemJsonWriterJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemJsonWriterJob")
                .repository(jobRepository)
                .start(itemJsonWriterStep())
                .build();
    }

    @Bean(value = "itemJsonWriterStep")
    public Step itemJsonWriterStep() {
        return this.stepBuilderFactory.get("itemJsonWriterStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJsonReader)
                .processor(getProcessor())
                .writer(itemJsonWriter)
                .build();
    }



    /**
     * 数据写到文件中 行格式
     * @param jobRepository
     * @return
     */
    @Bean("itemLineWriterJob")
    public Job itemLineWriterJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemLineWriterJob")
                .repository(jobRepository)
                .start(itemLineWriterStep())
                .build();
    }

    @Bean(value = "itemLineWriterStep")
    public Step itemLineWriterStep() {
        return this.stepBuilderFactory.get("itemLineWriterStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJsonReader)
                .processor(getProcessor())
                .writer(itemLineWriter)
                .build();
    }




    /**
     * JSON写数据
     * @param jobRepository
     * @return
     */
    @Bean("itemXmlWriterJob")
    public Job itemXmlWriterJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemXmlWriterJob")
                .repository(jobRepository)
                .start(itemXmlWriterStep())
                .build();
    }

    @Bean(value = "itemXmlWriterStep")
    public Step itemXmlWriterStep() {
        return this.stepBuilderFactory.get("itemXmlWriterStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJsonReader)
                .processor(getProcessor())
                .writer(itemXmlWriter)
                .build();
    }



    /**
     * JDBC写数据
     * @param jobRepository
     * @return
     */
    @Bean("itemJDBCWriterStep")
    public Job itemJDBCWriterJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemJDBCWriterJob")
                .repository(jobRepository)
                .start(itemJDBCWriterStep())
                .build();
    }

    @Bean(value = "itemJDBCWriterStep")
    public Step itemJDBCWriterStep() {
        return this.stepBuilderFactory.get("itemJDBCWriterStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJsonReader)
                .processor(getProcessor())
                .writer(itemJDBCWriter)
                .build();
    }


    /**
     * 处理
     * @return
     */
    private ItemProcessor<? super BatchEntity, ? extends BatchEntity> getProcessor() {
        return (ItemProcessor<BatchEntity, BatchEntity>) item -> {
            UUID uuid = UUID.randomUUID();
            item.setEntityId(uuid.toString());
            return item;
        };
    }


}
