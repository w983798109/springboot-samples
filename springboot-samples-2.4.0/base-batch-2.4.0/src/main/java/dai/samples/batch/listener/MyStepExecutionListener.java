package dai.samples.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * 最通用的Step执行侦听器。它允许在aStep开始之前和结束之后进行通知
 * @author daify
 * @date 2020-11-09
 */
@Slf4j
public class MyStepExecutionListener implements StepExecutionListener {


    @Override
    public void beforeStep(StepExecution stepExecution) {
        log.info("-------------beforeStep--------------");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.info("-------------afterStep--------------");
        return stepExecution.getExitStatus();
    }
}
