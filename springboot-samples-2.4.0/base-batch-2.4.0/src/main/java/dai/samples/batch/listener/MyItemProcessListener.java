package dai.samples.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.annotation.AfterProcess;
import org.springframework.batch.core.annotation.BeforeProcess;
import org.springframework.batch.core.annotation.OnProcessError;


/**
 * 处理任务的时候调用的监听器
 * @author daify
 * @date 2020-11-09
 */
@Slf4j
public class MyItemProcessListener {

    @BeforeProcess
    public void beforeProcess(Object o){
        log.info("-------------beforeProcess--------------");

    }

    @AfterProcess
    public void afterProcess(Object o, Object o1) {
        log.info("-------------afterProcess--------------");

    }

    @OnProcessError
    public void onProcessError(Object o, Exception e) {
        log.info("-------------onProcessError--------------");

    }
}
