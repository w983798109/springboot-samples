package dai.samples.batch.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author daify
 * @date 2020-11-03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "batch_entity")
public class BatchEntity {

    @Id
    private String entityId;

    /**
     * 初始名称
     */
    private String firstName;

    private String lastName;
    /**
     * 初始年龄
     */
    private int age;

    private String fullName;

    private boolean isAdult;

    private String helloMessage;

    public BatchEntity(String entityId,
                       int age,
                       String firstName,
                       boolean isAdult,
                       String lastName) {
        this.entityId = entityId;
        this.age = age;
        this.firstName = firstName;
        this.isAdult = isAdult;
        this.lastName = lastName;
    }
}