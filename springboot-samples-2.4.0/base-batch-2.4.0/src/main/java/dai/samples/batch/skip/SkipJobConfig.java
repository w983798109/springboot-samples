package dai.samples.batch.skip;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.FileNotFoundException;

/**
 * 跳过操作
 * @author daify
 * @date 2020-11-03
 */
@Slf4j
@Component
public class SkipJobConfig {

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    PlatformTransactionManager transactionManager;

    @Bean("skipJob10")
    public Job skipJob10(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("skipJob10")
                .repository(jobRepository)
                .start(skipStep10())
                .build();
    }

    @Bean("skipJob")
    public Job skipJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("skipJob")
                .repository(jobRepository)
                .start(skipStep())
                .build();
    }

    /**
     * 遇见
     * RuntimeException异常进行跳过，但是错误超过5次任务执行中断
     * FileNotFoundException异常不进行跳过，直接中断任务
     * @return
     */
    @Bean("skipStep")
    public Step skipStep() {
        return this.stepBuilderFactory.get("skipStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemReader())
                .processor(getExceptionProcessor())
                .writer(itemWriter())
                .faultTolerant()
                .skipLimit(5)
                .skip(RuntimeException.class)
                .noSkip(FileNotFoundException.class)
                .build();
    }

    /**
     * 遇见
     * RuntimeException异常进行跳过，但是错误超过10次任务执行中断
     * FileNotFoundException异常不进行跳过，直接中断任务
     * @return
     */
    @Bean("skipStep10")
    public Step skipStep10() {
        return this.stepBuilderFactory.get("skipStep")
                .<BatchEntity, BatchEntity>chunk(10)
                .reader(itemReader())
                .processor(getExceptionProcessor())
                .writer(itemWriter())
                .faultTolerant()
                .skipLimit(10)
                .skip(RuntimeException.class)
                .noSkip(FileNotFoundException.class)
                .build();
    }


    /**
     * JSON写数据
     * @return
     */
    public ItemWriter<BatchEntity> itemWriter() {
        String name = "skipJob-" + System.currentTimeMillis();
        String patch = "target/test-outputs/" + name + ".json";
        return new JsonFileItemWriterBuilder<BatchEntity>()
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(patch))
                .name(name)
                .build();
    }

    /**
     * JSON读数据
     * @return
     */
    public ItemReader<BatchEntity> itemReader() {
        return new JsonItemReaderBuilder<BatchEntity>()
                .jsonObjectReader(new JacksonJsonObjectReader<>(BatchEntity.class))
                .resource(new ClassPathResource("data/batchJob.json"))
                .name("batchJobReader")
                .build();
    }

    public ItemProcessor<? super BatchEntity, ? extends BatchEntity> getProcessor() {
        return new ChangeNameProcessor();
    }


    /**
     * 处理过程
     */
    class ChangeNameProcessor implements ItemProcessor<BatchEntity, BatchEntity> {

        @Override
        public BatchEntity process(BatchEntity person) {
            String fullName =person.getFirstName() + " " +
                    person.getLastName();
            person.setFullName(fullName);
            return person;
        }
    }

    public ItemProcessor<? super BatchEntity, ? extends BatchEntity> getExceptionProcessor() {
        return new ExceptionProcessor();
    }

    /**
     * 处理过程
     */
    class ExceptionProcessor implements ItemProcessor<BatchEntity, BatchEntity> {

        @Override
        public BatchEntity process(BatchEntity person) {
            if (person.getAge() < 12) {
                throw new RuntimeException("");
            }
            String fullName =person.getFirstName() + " " +
                    person.getLastName();
            person.setFullName(fullName);
            return person;
        }
    }
}
