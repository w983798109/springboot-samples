package dai.samples.batch.mock;

import com.alibaba.fastjson.JSON;
import dai.samples.batch.entity.BatchEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daify
 * @date 2020-11-03
 */
public class StartDataMock {

    public static List<BatchEntity> list = new ArrayList<>();

    static {
        initList ();
    }

    public static void initList () {
        list.clear();
        for (int i = 0; i < 10; i++) {
            BatchEntity entity = new BatchEntity();
            entity.setEntityId(String.valueOf(i));
            entity.setFirstName("firstName" + i);
            entity.setLastName("lastName" + i);
            entity.setAge(i * 2);
            list.add(entity);
        }
    }

    public static List<BatchEntity> listEnd = new ArrayList<>();

    public static void main(String[] args) {
        initList();
        System.out.println(JSON.toJSONString(list));
    }
}
