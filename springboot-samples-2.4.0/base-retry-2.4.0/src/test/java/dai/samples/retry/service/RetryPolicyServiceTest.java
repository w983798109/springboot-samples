package dai.samples.retry.service;

import dai.samples.retry.RetryApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RetryApplication.class})
@Slf4j
public class RetryPolicyServiceTest {

    @Autowired
    private RetryPolicyService retryPolicyService;

    @Test
    public void baseRetry() throws Throwable {
        retryPolicyService.baseRetryTemplate(1);
    }

}
