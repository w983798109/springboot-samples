package dai.samples.retry.service;

import dai.samples.retry.RetryApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author daify
 * @date 2021-01-04
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RetryApplication.class})
@Slf4j
public class RetryServiceTest {
    @Autowired
    private RetryService retryService;

    @Test
    public void baseRetry() throws Exception {
        int store = retryService.baseRetry(-1);
    }

    /**
     * 因为使用AOP 所以在同一个类中方法调用重试方法是无效的
     * @throws Exception
     */
    @Test
    public void baseRetryError() throws Exception {
        int store = retryService.baseRetryError(-1);
    }

    /**
     * 因为使用AOP 使用静态方法无效
     * @throws Exception
     */
    @Test
    public void baseRetryStatic() throws Exception {
        int store = retryService.baseRetryStatic(-1);
    }

}