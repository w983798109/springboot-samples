# springboot-samples

#### 介绍
springboot开发过程中，一些常用的组件整合

#### 项目说明
|项目名称|描述|地址|
|---|---|---|
|base-data-mybatis|整合mybatis-plus（实际上官方教程已经很多，只做了自定义插件）|未完成 |
|base-elasticsearch|整合elasticsearch| 未完成|
|base-jpa|JPA基础使用| [JPA 数据模型定义](https://blog.csdn.net/qq330983778/article/details/100000048)|
|base-jpa-query|JPA多表关联使用| [JPA 数据模型关联操作](https://blog.csdn.net/qq330983778/article/details/100026196)|
|base-kafka|kafka基础使用| [Spring Boot整合kafka整合1](https://blog.csdn.net/qq330983778/article/details/105937453);[Spring Boot整合kafka整合2](https://blog.csdn.net/qq330983778/article/details/105937612);[Spring Boot整合kafka整合3](https://blog.csdn.net/qq330983778/article/details/105937689)|
|base-log|日志配置| [SpringBoot日志配置](https://blog.csdn.net/qq330983778/article/details/98522530)|
|base-rabbit|rabbitMQ简单使用|[RabbitMQ基础使用](https://blog.csdn.net/qq330983778/article/details/99479526) |
|base-rabbit3|rabbitMQ一些自定义配置|[消息确认回调、消息转换以及消息异常处理](https://blog.csdn.net/qq330983778/article/details/99611193) |
|base-rabbit-delay|rabbitMQ延时队列|[延时队列和消息重试](https://blog.csdn.net/qq330983778/article/details/99661012) |
|base-redis|redis简单使用| [RedisTemplate基础使用](https://blog.csdn.net/qq330983778/article/details/98786268);[Redis实现简单的发布订阅以及配置序列化方式](https://blog.csdn.net/qq330983778/article/details/98900970)|
|base-redis-lock|redis分布式锁| [Redis分布式锁的简单实现](https://blog.csdn.net/qq330983778/article/details/99234635)|
|base-redis-delay|基于有赞的延时消息方案的简单实现| [延时队列的简单实现](https://blog.csdn.net/qq330983778/article/details/99341671)|
|base-swagger|swagger使用| [wagger2使用](https://blog.csdn.net/qq330983778/article/details/98665726)|
|base-mongodb|mongodb简单使用|[MongoDB安装以及Spring Boot整合](https://blog.csdn.net/qq330983778/article/details/104079833),[MongoDB实体创建以及简单CRUD](https://blog.csdn.net/qq330983778/article/details/104079843),[MongoDB聚合操作](https://blog.csdn.net/qq330983778/article/details/104079851),[MongoDB分组去重以及MongoDB联表查询](https://blog.csdn.net/qq330983778/article/details/104079857) |
|base-cache|spring-cache简单使用|[Spring Cache的整合和注解介绍](https://blog.csdn.net/qq330983778/article/details/109140787)|
|base-cache-config|spring-cache使用redis|[Spring Cache使用Redis缓存、自定义注解参数值](https://blog.csdn.net/qq330983778/article/details/109140802)|
|base-batch-2.4.0|base-batch的相关使用|[Spring Boot 整合——Spring batch基本使用](https://blog.csdn.net/qq330983778/article/details/111824575)|
|base-mobile|spring识别设备类型的小工具|[Spring Mobile用来识别请求设备类型的小工具](https://blog.csdn.net/qq330983778/article/details/115049194) |
|base-shell|使用Spring Shell开发java命令行工具|[使用Spring Shell开发java命令行工具](https://blog.csdn.net/qq330983778/article/details/115267430) |


#### 博客地址

[个人博客](https://blog.csdn.net/qq330983778)


