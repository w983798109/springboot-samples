package dai.samples.kafka.converter.config;

import com.alibaba.fastjson.JSON;
import dai.samples.kafka.common.entity.CustomMessage;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * 获取消费的异步结果
 * @author daify
 */
@Log4j2
public class CustomListenableFutureCallback implements ListenableFutureCallback<SendResult<String, Object>> {

    public CustomListenableFutureCallback(CustomMessage customMessage) {
        this.customMessage = customMessage;
    }

    private CustomMessage customMessage;

    /**
     * 失败的时候
     * @param throwable
     */
    @Override
    public void onFailure(Throwable throwable) {
        log.error("执行了onFailure");
        log.error("topic:{},message:{}" ,
            customMessage.getPayload(),
            JSON.toJSONString(customMessage.getPayload()));
    }

    /**
     * 成功的办法
     * @param stringMyMessageSendResult
     */
    @Override
    public void onSuccess(SendResult<String, Object> stringMyMessageSendResult) {
        log.info("执行了onSuccess");
        log.info("topic:{},message:{}" ,
            customMessage.getPayload(),
            JSON.toJSONString(customMessage.getPayload()));
    }
}
