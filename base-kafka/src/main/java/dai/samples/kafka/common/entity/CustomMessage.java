package dai.samples.kafka.common.entity;


import lombok.Data;
import org.springframework.messaging.MessageHeaders;


@Data
public class CustomMessage implements SamplesMessage {

    private Object payLoad;

    private String topic;

    public void setPayload(Object payLoad,String topic) {
        this.payLoad = payLoad;
        this.topic = topic;
    }

    @Override
    public String getTopic() {
        return this.topic;
    }

    @Override
    public Object getPayload() {
        return this.payLoad;
    }

    @Override
    public MessageHeaders getHeaders() {
        return new MessageHeaders(null);
    }


}
