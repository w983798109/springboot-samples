package dai.samples.shiro.filter;

import lombok.extern.log4j.Log4j2;
import org.apache.shiro.web.filter.PathMatchingFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 仅仅是个测试
 */
@Log4j2
public class TestPathMatchingFilter extends PathMatchingFilter {


    public TestPathMatchingFilter() {
    }

    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) {
       log.info("TestPathMatchingFilter onPreHandle");
       return true;
    }


}
