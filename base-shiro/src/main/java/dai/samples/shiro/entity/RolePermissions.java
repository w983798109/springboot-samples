package dai.samples.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色ID和权限ID关系
 */
@Data
@TableName
public class RolePermissions implements Serializable {

    /**
     * 记录ID
     */
    @TableId
    private long rpId;
    /**
     * 角色ID
     */
    private long roleId;
    /**
     * 权限ID
     */
    private long permissionsId;
}
