package dai.samples.mongodb.service;

import dai.samples.mongodb.bean.BucketVo;
import dai.samples.mongodb.bean.GroupVo;

import java.util.List;

public interface AggregationService {

    /**
     * 根据"type"对订单进行分组
     * @return
     */
    List<BucketVo> getAggBucket();

    /**
     * 聚合操作
     * @return
     */
    List<GroupVo> getAggGroup();

    /**
     * 指定返回字段
     * @return
     */
    List<GroupVo> getAggProject();

    /**
     * 将数据根据字段分为指定数量的组
     * @return
     */
    List<BucketVo> getAggBucketAuto();

    /**
     * 使用两次分组进行去重求数量
     * @return
     */
    List<GroupVo> getAggGroupDeDuplication();
}
