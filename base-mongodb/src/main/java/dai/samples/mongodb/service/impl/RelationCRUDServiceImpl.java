package dai.samples.mongodb.service.impl;

import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.entity.UserInfo;
import dai.samples.mongodb.repository.OrderRepository;
import dai.samples.mongodb.repository.UserInfoRepository;
import dai.samples.mongodb.service.RelationCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 嵌套结构的相关例子
 */
@Service
public class RelationCRUDServiceImpl implements RelationCRUDService {

    @Autowired
    private UserInfoRepository infoRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public void saveUserInfo(UserInfo userInfo) {
        infoRepository.save(userInfo);
    }

    @Override
    public void saveOrder(Order order) {
        orderRepository.save(order);
    }

    @Override
    public UserInfo queryUserInfo(String id) {
        return infoRepository.findById(id).get();
    }

    @Override
    public Order queryOrder(String id) {
        return orderRepository.findById(id).get();
    }

    @Override
    public void deleteUserInfo(String id) {
        infoRepository.deleteById(id);
    }

    @Override
    public void deleteOrder(String id) {
        orderRepository.deleteById(id);
    }
}
