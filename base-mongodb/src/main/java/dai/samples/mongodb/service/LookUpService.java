package dai.samples.mongodb.service;

import dai.samples.mongodb.bean.OrderAndUserVo;
import dai.samples.mongodb.bean.OrderVo;
import dai.samples.mongodb.bean.UserGroupVo;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface LookUpService {

    /**
     * 联表查询
     * @return
     */
    List<OrderVo> queryOrderAndUserInfo();

    /**
     * 联表查询
     * @return
     */
    List<OrderAndUserVo> queryOrderUserInfo();

    /**
     * 联表查询
     * @return
     */
    List<UserGroupVo> queryUserGroup();


    /**
     * 联表查询
     * @return
     */
    List<OrderVo> queryOrderAndUserInfoSort(Sort.Direction direction);
}
