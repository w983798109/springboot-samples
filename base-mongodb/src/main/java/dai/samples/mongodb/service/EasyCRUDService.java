package dai.samples.mongodb.service;

import dai.samples.mongodb.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 新增和更新demo
 */
public interface EasyCRUDService {

    /**
     * 单一保存
     * @param product
     */
    void saveOne(Product product);

    /**
     * 批量保存
     * @param products
     */
    void batchSave(List<Product> products);

    /**
     * 单一更新
     * @param product
     */
    void updateOne(Product product);

    /**
     * 批量更新
     * @param products
     */
    void batchUpdate(List<Product> products);

    /**
     * 单一查询
     * @param id
     */
    Product queryById(String id);

    /**
     * 根据实体查询
     * @param product
     */
    List<Product> queryByEntity(Product product);

    /**
     * 根据实体分页查询
     * @param product
     */
    Page<Product> pageByEntity(Product product, int page, int size);

    /**
     * 单一删除
     * @param id
     */
    void deleteOne(String id);

    /**
     * 批量删除
     * @param idList
     */
    void batchDelete(List<String> idList);


}
