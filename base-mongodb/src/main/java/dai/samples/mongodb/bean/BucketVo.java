package dai.samples.mongodb.bean;

import lombok.Data;

@Data
public class BucketVo {

    private String id;

    private int count;

    private int sum;

    private double avg;

}
