package dai.samples.mongodb.bean;

import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.entity.UserInfo;
import lombok.Data;

@Data
public class OrderVo extends Order {

    private UserInfo userInfo;
}
