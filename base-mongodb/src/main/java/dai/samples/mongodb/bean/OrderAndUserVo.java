package dai.samples.mongodb.bean;

import dai.samples.mongodb.entity.Order;
import lombok.Data;

@Data
public class OrderAndUserVo extends Order {

    private String userName;

}
