package dai.samples.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Data
@Document(collection = "UserInfo")
public class UserInfo {

    @Indexed
    private String id;

    private String userName;

    private int type;

    private String groupName;

    @DBRef
    private UserLogistics logistics;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserInfo)) return false;
        UserInfo userInfo = (UserInfo) o;
        return Objects.equals(getId(), userInfo.getId()) &&
            Objects.equals(getUserName(), userInfo.getUserName()) &&
            Objects.equals(getLogistics(), userInfo.getLogistics());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserName(), getLogistics());
    }
}
