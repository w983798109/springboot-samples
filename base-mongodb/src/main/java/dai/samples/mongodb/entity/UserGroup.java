package dai.samples.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Data
@Document(collection = "UserGroup")
public class UserGroup {

    @Indexed
    private String id;

    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserGroup)) return false;
        UserGroup userGroup = (UserGroup) o;
        return Objects.equals(getId(), userGroup.getId()) &&
            Objects.equals(getName(), userGroup.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }
}
