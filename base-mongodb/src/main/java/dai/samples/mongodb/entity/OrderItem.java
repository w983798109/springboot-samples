package dai.samples.mongodb.entity;

import lombok.Data;

import java.util.Objects;

@Data
public class OrderItem {

    private String id;

    private String orderId;

    private String productId;

    private int total;

    private double money;

    private double totalMoney;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderItem)) return false;
        OrderItem orderItem1 = (OrderItem) o;
        return Objects.equals(getId(), orderItem1.getId()) &&
            Objects.equals(getOrderId(), orderItem1.getOrderId()) &&
            Objects.equals(getProductId(), orderItem1.getProductId()) &&
            Objects.equals(getTotal(), orderItem1.getTotal()) &&
            Objects.equals(getMoney(), orderItem1.getMoney()) &&
            Objects.equals(getTotalMoney(), orderItem1.getTotalMoney());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getOrderId(), getProductId(), getTotal(), getMoney(), getTotalMoney());
    }
}
