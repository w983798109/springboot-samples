package dai.samples.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Data
@Document(collection = "UserLogistics")
public class UserLogistics {

    @Indexed
    private String id;

    private String address;

    private String tel;

    private int type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLogistics)) return false;
        UserLogistics that = (UserLogistics) o;
        return getType() == that.getType() &&
            Objects.equals(getId(), that.getId()) &&
            Objects.equals(getAddress(), that.getAddress()) &&
            Objects.equals(getTel(), that.getTel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAddress(), getTel(), getType());
    }
}
