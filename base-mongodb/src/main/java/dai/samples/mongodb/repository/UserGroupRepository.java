package dai.samples.mongodb.repository;

import dai.samples.mongodb.entity.UserGroup;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserGroupRepository extends MongoRepository<UserGroup,String> {

}
