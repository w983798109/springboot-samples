package dai.samples.mongodb.repository;

import dai.samples.mongodb.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order,String> {

}
