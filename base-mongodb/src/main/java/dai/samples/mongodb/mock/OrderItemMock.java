package dai.samples.mongodb.mock;


import dai.samples.mongodb.entity.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class OrderItemMock {

    public static OrderItem createMock(int id, String orderId, String productId) {
        OrderItem item = new OrderItem();
        item.setId(String.valueOf(id));
        item.setMoney(10.0d * id);
        item.setOrderId(orderId);
        item.setProductId(productId);
        item.setTotal(id);
        item.setTotalMoney(10.0d * id * id);
        return item;
    }

    public static List<OrderItem> createMockList(int num,String orderId,String productId) {
        List<OrderItem> rest = new ArrayList<>();
        for (int i = 1; i <= num; i++) {
            rest.add(createMock(i,orderId,productId));
        }
        return rest;
    }

}
