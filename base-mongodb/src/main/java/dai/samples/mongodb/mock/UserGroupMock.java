package dai.samples.mongodb.mock;


import dai.samples.mongodb.entity.UserGroup;

public class UserGroupMock {

    public static UserGroup createMock(int id, String name, String userName) {
        UserGroup group = new UserGroup();
        group.setId(String.valueOf(id));
        group.setName(name);
        return group;
    }
}
