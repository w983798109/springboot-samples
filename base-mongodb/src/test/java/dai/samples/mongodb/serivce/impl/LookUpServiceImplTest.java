package dai.samples.mongodb.serivce.impl;

import dai.samples.MongodbApplication;
import dai.samples.mongodb.bean.OrderAndUserVo;
import dai.samples.mongodb.bean.OrderVo;
import dai.samples.mongodb.bean.UserGroupVo;
import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.entity.OrderItem;
import dai.samples.mongodb.entity.UserGroup;
import dai.samples.mongodb.entity.UserInfo;
import dai.samples.mongodb.mock.OrderItemMock;
import dai.samples.mongodb.mock.OrderMock;
import dai.samples.mongodb.mock.UserGroupMock;
import dai.samples.mongodb.mock.UserInfoMock;
import dai.samples.mongodb.repository.OrderRepository;
import dai.samples.mongodb.repository.UserGroupRepository;
import dai.samples.mongodb.repository.UserInfoRepository;
import dai.samples.mongodb.service.LookUpService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 联表查询
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongodbApplication.class)
@Slf4j
public class LookUpServiceImplTest {

    @Autowired
    LookUpService lookUpService;
    @Autowired
    UserInfoRepository userInfoRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserGroupRepository userGroupRepository;

    /**
     * 使用ID进行关联的表关联查询
     */
    @Test
    public void queryOrderAndUserInfo() {
        // 清除干扰数据
        userInfoRepository.deleteAll();
        orderRepository.deleteAll();
        // 添加mock数据
        UserInfo mock1 = UserInfoMock.createMock(10, 1);
        userInfoRepository.save(mock1);
        List<OrderItem> mockList = OrderItemMock.createMockList(1, "1", "1");
        Order mock = OrderMock.createMock(1, mock1.getId(), 1, mockList);
        orderRepository.save(mock);
        List<OrderVo> orderVos = lookUpService.queryOrderAndUserInfo();
        Assert.assertTrue(!CollectionUtils.isEmpty(orderVos));
        Assert.assertTrue(orderVos.get(0).getUserInfo().equals(mock1));
    }

    /**
     * 使用ID进行关联的表关联查询
     */
    @Test
    public void queryOrderUserInfo() {
        // 清除干扰数据
        userInfoRepository.deleteAll();
        orderRepository.deleteAll();
        // 添加mock数据
        UserInfo mock1 = UserInfoMock.createMock(10, 1);
        userInfoRepository.save(mock1);
        List<OrderItem> mockList = OrderItemMock.createMockList(1, "1", "1");
        Order mock = OrderMock.createMock(1, mock1.getId(), 1, mockList);
        orderRepository.save(mock);
        List<OrderAndUserVo> orderVos = lookUpService.queryOrderUserInfo();
        Assert.assertTrue(!CollectionUtils.isEmpty(orderVos));
        Assert.assertTrue(!StringUtils.isEmpty(orderVos.get(0).getUserName()));
    }

    /**
     * 使用非ID字段进行关联查询的表关联查询
     */
    @Test
    public void queryUserGroup() {
        // 清除干扰数据
        userInfoRepository.deleteAll();
        userGroupRepository.deleteAll();
        // 添加mock数据
        UserGroup group1 = UserGroupMock.createMock(1, "分组1","测试1");
        userGroupRepository.save(group1);
        UserInfo mock1 = UserInfoMock.createMock(1, group1.getName());
        userInfoRepository.save(mock1);
        UserInfo mock2 = UserInfoMock.createMock(2, group1.getName());
        userInfoRepository.save(mock2);
        UserInfo mock3 = UserInfoMock.createMock(3, group1.getName());
        userInfoRepository.save(mock3);
        List<UserGroupVo> userGroupVos = lookUpService.queryUserGroup();
        Assert.assertNotNull(userGroupVos);
        Assert.assertNotNull(userGroupVos.get(0).getUserInfos());
        Assert.assertTrue(userGroupVos.get(0).getUserInfos().size() == 3);

    }

    /**
     * 针对关联数据中的内容进行排序或者筛选操作
     */
    @Test
    public void queryOrderAndUserInfoSort() {
        // 清除干扰数据
        userInfoRepository.deleteAll();
        orderRepository.deleteAll();
        // 添加mock数据
        UserInfo mock1 = UserInfoMock.createMock(1, 1);
        Order mock01 = OrderMock.createMock(1, 1, mock1.getId());
        userInfoRepository.save(mock1);
        orderRepository.save(mock01);

        UserInfo mock2 = UserInfoMock.createMock(2, 2);
        Order mock02 = OrderMock.createMock(2, 1, mock2.getId());
        userInfoRepository.save(mock2);
        orderRepository.save(mock02);

        UserInfo mock3 = UserInfoMock.createMock(3, 3);
        Order mock03 = OrderMock.createMock(3, 1, mock3.getId());
        userInfoRepository.save(mock3);
        orderRepository.save(mock03);

        UserInfo mock4 = UserInfoMock.createMock(4, 4);
        Order mock04 = OrderMock.createMock(4, 1, mock4.getId());
        userInfoRepository.save(mock4);
        orderRepository.save(mock04);

        List<OrderVo> orderVos1 = lookUpService.queryOrderAndUserInfoSort(Sort.Direction.ASC);
        Assert.assertNotNull(orderVos1);
        Assert.assertTrue(orderVos1.size() == 4);
        // 升序
        Assert.assertTrue(orderVos1.get(0).getUserInfo().getType() -
                                    orderVos1.get(1).getUserInfo().getType() < 0);



        List<OrderVo> orderVos2 = lookUpService.queryOrderAndUserInfoSort(Sort.Direction.DESC);
        Assert.assertNotNull(orderVos2);
        Assert.assertTrue(orderVos2.size() == 4);
        // 降序
        Assert.assertTrue(orderVos2.get(0).getUserInfo().getType() -
                                    orderVos2.get(1).getUserInfo().getType() > 0);

    }
}