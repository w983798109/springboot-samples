package dai.samples.mongodb.serivce.impl;

import dai.samples.MongodbApplication;
import dai.samples.mongodb.bean.BucketVo;
import dai.samples.mongodb.bean.GroupVo;
import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.mock.OrderMock;
import dai.samples.mongodb.repository.OrderRepository;
import dai.samples.mongodb.service.AggregationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


/**
 * 聚合操作
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongodbApplication.class)
@Slf4j
public class AggregationServiceImplTest {

    @Autowired
    AggregationService aggregationService;

    @Autowired
    OrderRepository orderRepository;

    /**
     * 根据"type"对订单进行分组
     */
    @Test
    public void getAggBucket() {
        // 清除干扰数据
        orderRepository.deleteAll();
        List<Order> insert = new ArrayList<>();
        // 创建mock订单 订单总金额10.0 订单产品数1
        Order mock1 = OrderMock.createMock(1, 1);
        insert.add(mock1);
        // 创建mock订单 订单总金额20.0 订单产品数2
        Order mock2 = OrderMock.createMock(2, 1);
        insert.add(mock2);

        // 创建mock订单 订单总金额30.0 订单产品数3
        Order mock3 = OrderMock.createMock(3, 2);
        insert.add(mock3);
        // 创建mock订单 订单总金额40.0 订单产品数4
        Order mock4 = OrderMock.createMock(4, 2);
        insert.add(mock4);

        // 创建mock订单 订单总金额50.0 订单产品数5
        Order mock5 = OrderMock.createMock(5, 3);
        insert.add(mock5);
        // 创建mock订单 订单总金额60.0 订单产品数6
        Order mock6 = OrderMock.createMock(6, 3);
        insert.add(mock6);
        orderRepository.saveAll(insert);
        // 根据type分组 [0,1),[1,2),[2,3),[3,other)分组，求产品和，平均金额
        List<BucketVo> aggBucket = aggregationService.getAggBucket();
        Assert.assertNotNull(aggBucket);
        // 不存在[0,1) 这一组
        Assert.assertTrue(aggBucket.size() == 3);
        // 比对[2,3)这一组数据 平均金额
        Assert.assertTrue(aggBucket.get(1).getAvg() == 35.0D);
        // 比对[3,other)这一组数据 产品和
        Assert.assertTrue(aggBucket.get(2).getSum() == 11);
    }

    /**
     * 根据聚合
     */
    @Test
    public void getAggGroup() {
        // 清除干扰数据
        orderRepository.deleteAll();
        List<Order> insert = new ArrayList<>();
        // 创建mock订单 订单总金额10.0 订单产品数1
        Order mock1 = OrderMock.createMock(1, 1,"1");
        insert.add(mock1);
        // 创建mock订单 订单总金额20.0 订单产品数2
        Order mock2 = OrderMock.createMock(2, 1,"1");
        insert.add(mock2);

        // 创建mock订单 订单总金额30.0 订单产品数3
        Order mock3 = OrderMock.createMock(3, 2,"1");
        insert.add(mock3);
        // 创建mock订单 订单总金额40.0 订单产品数4
        Order mock4 = OrderMock.createMock(4, 2,"2");
        insert.add(mock4);
        orderRepository.saveAll(insert);
        // 根据用户id和类型进行分组，求不同用户不同类型订单数量，以及订单中产品数量的汇总
        List<GroupVo> aggGroup = aggregationService.getAggGroup();

        Assert.assertNotNull(aggGroup);
        // 用户1 根据type分两组，用户2根据type分1组
        Assert.assertTrue(aggGroup.size() == 3);
        // 第三条数默认为用户2
        Assert.assertTrue("1".equals(aggGroup.get(2).getUserId()));
        // 用户1 类型1 商品总数为1 + 2
        Assert.assertTrue(aggGroup.get(2).getCount() == 3);

    }

    /**
     * 指定返回字段
     */
    @Test
    public void getAggProject() {
        // 清除干扰数据
        orderRepository.deleteAll();
        List<Order> insert = new ArrayList<>();
        // 创建mock订单 订单总金额10.0 订单产品数1
        Order mock1 = OrderMock.createMock(1, 1,"1");
        insert.add(mock1);
        // 创建mock订单 订单总金额20.0 订单产品数2
        Order mock2 = OrderMock.createMock(2, 1,"1");
        insert.add(mock2);

        // 创建mock订单 订单总金额30.0 订单产品数3
        Order mock3 = OrderMock.createMock(3, 2,"1");
        insert.add(mock3);
        // 创建mock订单 订单总金额40.0 订单产品数4
        Order mock4 = OrderMock.createMock(4, 2,"2");
        insert.add(mock4);
        orderRepository.saveAll(insert);
        // 根据用户id和类型进行分组，求不同用户不同类型订单数量，以及订单中产品数量的汇总
        List<GroupVo> aggGroup = aggregationService.getAggProject();

        Assert.assertNotNull(aggGroup);
        // 用户1 根据type分两组，用户2根据type分1组
        Assert.assertTrue(aggGroup.size() == 3);
        // Project中设置了没有返回Count的内容，所以此字段应该为空
        Assert.assertTrue(aggGroup.get(2).getCount() == 0);
    }

    /**
     * 将数据根据字段分为指定数量的组
     */
    @Test
    public void getAggBucketAuto() {
        // 清除干扰数据
        orderRepository.deleteAll();
        List<Order> insert = new ArrayList<>();
        // 创建mock订单 订单总金额10.0 订单产品数1
        Order mock1 = OrderMock.createMock(1, 1);
        insert.add(mock1);
        // 创建mock订单 订单总金额20.0 订单产品数2
        Order mock2 = OrderMock.createMock(2, 1);
        insert.add(mock2);

        // 创建mock订单 订单总金额30.0 订单产品数3
        Order mock3 = OrderMock.createMock(3, 2);
        insert.add(mock3);
        // 创建mock订单 订单总金额40.0 订单产品数4
        Order mock4 = OrderMock.createMock(4, 2);
        insert.add(mock4);

        // 创建mock订单 订单总金额50.0 订单产品数5
        Order mock5 = OrderMock.createMock(5, 3);
        insert.add(mock5);
        // 创建mock订单 订单总金额60.0 订单产品数6
        Order mock6 = OrderMock.createMock(6, 3);
        insert.add(mock6);
        orderRepository.saveAll(insert);
        // 根据type分组 此时被默认强制分为2组
        List<BucketVo> aggBucket = aggregationService.getAggBucketAuto();
        Assert.assertNotNull(aggBucket);
        // 此时被默认强制分为2组
        Assert.assertTrue(aggBucket.size() == 2);
    }

    /**
     * 使用两次分组进行去重求数量
     */
    @Test
    public void getAggGroupDeDuplication() {
        // 清除干扰数据
        orderRepository.deleteAll();
        List<Order> insert = new ArrayList<>();
        // 创建mock订单 订单总金额10.0 订单产品数1
        Order mock1 = OrderMock.createMock(1, 1,"1");
        insert.add(mock1);
        // 创建mock订单 订单总金额20.0 订单产品数2
        Order mock2 = OrderMock.createMock(2, 1,"1");
        insert.add(mock2);

        // 创建mock订单 订单总金额30.0 订单产品数3
        Order mock3 = OrderMock.createMock(3, 2,"1");
        insert.add(mock3);
        // 创建mock订单 订单总金额40.0 订单产品数4
        Order mock4 = OrderMock.createMock(4, 2,"1");
        insert.add(mock4);
        orderRepository.saveAll(insert);

        // 一种分组排除重复的用法，当我们存在不同的用户不同类型的数据的时，使用用户和类型分组只能得到
        // 同一用户下不同类型的数据总数或者其他聚合数据。但是没得得到同一用户到底有几种类型。
        // 但mongodb的aggregate类似于管道的处理，下一步处理是基于上一步的结果进行的，
        // 可以在分组得到结果后再次使用分组通过两次分组获得需要的结果
        List<GroupVo> aggGroupDeDuplication = aggregationService.getAggGroupDeDuplication();
        Assert.assertNotNull(aggGroupDeDuplication);
        // 此时被默认强制分为2组
        Assert.assertTrue(aggGroupDeDuplication.get(0).getCount() == 2);

    }
}