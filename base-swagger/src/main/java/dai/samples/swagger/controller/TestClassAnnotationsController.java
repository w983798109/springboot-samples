package dai.samples.swagger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;

/**
 * 测试Controller注解
 * @author daify
 * @date 2019-07-16 10:57
 **/
@Api(value = "控制器类型",
        tags = "测试控制器类型注解内容")
@RestController
@RequestMapping("class")
public class TestClassAnnotationsController {

    /**
     * 一个基础的测试
     * @param request
     * @return
     */
    @ApiOperation(value="测试基础对象", notes="测试基础对象备注",httpMethod = "POST")
    @RequestMapping(value = "base",method = RequestMethod.POST)
    public String base(ServletRequest request) {
        String contentType = request.getContentType();
        return "contentType：" + contentType;
    }

}
