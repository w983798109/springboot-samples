package dai.samples.swagger.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.swagger.bean.QueryBaseRequest;
import dai.samples.swagger.bean.QueryUserRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试模型对象注解
 * @author daify
 * @date 2019-07-15 17:14
 **/
@Api(value = "测试模型注解",tags = "测试数据模型上的注解内容")
@RestController
@RequestMapping("model")
public class TestModelAnnotationsController {

    /**
     * 入参添加注释
     * @param request
     * @return
     */
    @ApiOperation(value="测试基础对象", notes="测试基础对象备注",httpMethod = "POST")
    @RequestMapping(value = "base",method = RequestMethod.POST)
    public String base(QueryBaseRequest request) {
        String s = JSON.toJSONString(request);
        return "请求内容：" + s;
    }

    /**
     * 测试对象类型继承
     * @param request
     * @return
     */
    @ApiOperation(value="测试子类对象", notes="测试子类对象备注",httpMethod = "POST")
    @RequestMapping(value = "son",method = RequestMethod.POST)
    public String base(QueryUserRequest request) {
        String s = JSON.toJSONString(request);
        return "请求内容：" + s;
    }
}
