package dai.samples.swagger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;

/**
 * @author daify
 * @date 2019-08-05
 */
@Api
@RestController
@RequestMapping("BaseSwagger")
public class BaseSwaggerController {

    /**
     * 一个基础的测试
     * @param request
     * @return
     */
    @ApiOperation(value="测试基础对象")
    @RequestMapping(value = "base",method = RequestMethod.POST)
    public String base(ServletRequest request) {
        String contentType = request.getContentType();
        return "contentType：" + contentType;
    }
}
