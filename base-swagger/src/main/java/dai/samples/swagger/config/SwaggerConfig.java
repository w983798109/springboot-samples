package dai.samples.swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 *
 * @author daify
 * @date 2019-07-15 9:22
 **/
@Configuration
public class SwaggerConfig {

    /**
     * 创建swagger运行的docket环境
     * @return
     */
    @Bean
    public Docket createRestApi() {
        // 使用Docket启动，
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // 扫描基础包
                .apis(RequestHandlerSelectors.basePackage("dai.samples.swagger"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 创建swagger的api信息
     * @return
     */
    private ApiInfo apiInfo() {
        // 配置名称地址和email
        Contact contact = new Contact("大·风","https://blog.csdn.net/qq330983778","email");
        return new ApiInfoBuilder()
                // 标题
                .title("Spring Boot 整合Swagger")
                // 描述
                .description("这个一个demo项目")
                .termsOfServiceUrl("https://blog.csdn.net/qq330983778")
                .version("1.0")
                .contact(contact)
                .build();
    }
    
    
}
