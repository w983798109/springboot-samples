package dai.samples.cache.dao;

import dai.samples.cache.CacheApplication;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * @author daify
 * @date 2020-10-17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CacheApplication.class)
@EnableAutoConfiguration
@Slf4j
public class CacheDaoTest extends CacheApplication {

    @Autowired
    private CacheDao cacheDao;

    private static String KEY ="test";


    /**
     * 获取数据
     * @return
     */
    @Test
    public void find() {
        cacheDao.clear(KEY);
        String cacheData = cacheDao.find(KEY);
        Assert.assertTrue(StringUtils.isEmpty(cacheData));

        cacheDao.putStr("123",KEY);
        String cacheData2 = cacheDao.find(KEY);
        Assert.assertTrue("123".equals(cacheData2));
        log.info("测试执行完毕");
    }


}
