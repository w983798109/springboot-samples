package dai.samples.jpa.entity.base;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 行注解测试
 * @author daify
 * @date 2019-07-20
 */
@Data
@Entity
@Table(name = "base_column_bean",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"col_unique","col_unique_v2"})
        })
public class ColumnBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "value",// 映射的列名
            unique = true,// 是否唯一
            nullable = false,// 是否允许为空
            length = 150,// 对于字符型列，length属性指定列的最大字符长度
            insertable = true,// 是否允许插入
            updatable = false)// 是否允许更新
    private String value;

    @Column(name = "null_value",// 映射的列名
            unique = false,// 是否唯一
            nullable = true,// 是否允许为空
            length = 150,// 对于字符型列，length属性指定列的最大字符长度
            insertable = false,// 是否允许插入
            updatable = false)// 是否允许更新
    private String nullValue;

    @Column(name = "num",// 映射的列名
            length = 150,// 对于字符型列，length属性指定列的最大字符长度
            precision = 10,// 字段的精度
            scale = 3)// 字段的小数点位数
    private Double num;

    /**
     * 用来测试basic的懒加载
     */
    private String testBasic;

    /**
     * 用来测试basic的正常加载
     */
    private String testBasicV2;

    /**
     * 用来测试Transient
     */
    @Transient
    private String notInTable;

    /**
     * 测试时间的
     */
    private Date createTime;

    /**
     * 测试唯一约束
     */
    @Column(name = "col_unique")
    private String colUnique;
    /**
     * 测试唯一约束
     */
    @Column(name = "col_unique_v2")
    private String colUniqueV2;

    @Lob
    private String bigText;

    @Version
    private Long version;


    @Basic(fetch = FetchType.LAZY,
            optional = false)
    public String getTestBasic() {
        System.out.println("执行了getTestBasic方法");
        return testBasic;
    }

    public void setTestBasic(String testBasic) {
        this.testBasic = testBasic;
    }

    @Basic(fetch = FetchType.EAGER,
            optional = true)
    public String getTestBasicV2() {
        System.out.println("执行了getTestBasicV2方法");
        return testBasicV2;
    }

    public void setTestBasicV2(String testBasicV2) {
        this.testBasicV2 = testBasicV2;
    }

    /**
     * 表示该属性并不是一个到数据库表的字段的映射，指定的这些属性不会被持久化
     * @return
     */
    @Transient
    public String getNotInTable() {
        return notInTable;
    }

    public void setNotInTable(String notInTable) {
        this.notInTable = notInTable;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
