package dai.samples.jpa.entity.ext;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author daify
 * @date 2019-07-29 17:59
 **/
@Data
@Entity
@Table(name = "ext_son_v2_join_bean")
public class SonV2JoinedBean extends BaseJoinedBean {
    
    @Basic
    private String sonV2Value;
}
