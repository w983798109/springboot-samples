package dai.samples.jpa.entity.ext;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author daify
 * @date 2019-07-20
 */
@Data
@Entity
@DiscriminatorValue("son")
public class SonSingleBean extends BaseSingleBean {

    @Basic
    private String sonValue;
}
