package dai.samples.jpa.entity.ext;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author daify
 * @date 2019-07-29 18:13
 **/
@Data
@Entity
@Table(name = "ext_son_table_per_bean")
public class SonTablePerBean extends BaseTablePerBean{

    @Basic
    private String sonValue;
}
