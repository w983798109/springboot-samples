package dai.samples.jpa.repository.key;

import dai.samples.jpa.entity.key.KeysBean;
import dai.samples.jpa.entity.key.TestKeys;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-29 16:11
 **/
public interface TestKeysRepository extends JpaRepository <TestKeys, KeysBean> {
}
