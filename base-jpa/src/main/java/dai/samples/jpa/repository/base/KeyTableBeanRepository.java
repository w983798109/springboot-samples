package dai.samples.jpa.repository.base;

import dai.samples.jpa.entity.base.KeyTableBean;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-29 14:52
 **/
public interface KeyTableBeanRepository extends JpaRepository <KeyTableBean, Long> {
}
