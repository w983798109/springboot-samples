package dai.samples.jpa.repository.ext;

import dai.samples.jpa.entity.ext.BaseJoinedBean;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-30 9:09
 **/
public interface BaseJoinedBeanRepository 
        extends JpaRepository <BaseJoinedBean, Long> {
}
