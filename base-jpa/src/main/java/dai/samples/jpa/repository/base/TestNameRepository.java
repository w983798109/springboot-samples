package dai.samples.jpa.repository.base;

import dai.samples.jpa.entity.base.TestName;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-29 14:53
 **/
public interface TestNameRepository extends JpaRepository <TestName, Long> {
}
