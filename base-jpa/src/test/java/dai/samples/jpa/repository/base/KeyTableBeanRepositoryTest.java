package dai.samples.jpa.repository.base;

import com.alibaba.fastjson.JSON;
import dai.samples.jpa.JpaApplication;
import dai.samples.jpa.entity.base.KeyTableBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;


/**
 *
 * @author daify
 * @date 2019-07-29 15:25
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class KeyTableBeanRepositoryTest {
    
    @Autowired
    KeyTableBeanRepository repository;

    @Before
    public void initTable() {
        repository.deleteAll();
    }

    @Test
    public void add() {
        KeyTableBean bean = new KeyTableBean();
        bean.setValue("value");
        repository.save(bean);
        KeyTableBean bean2 = new KeyTableBean();
        bean2.setValue("value2");
        repository.save(bean2);
        List <KeyTableBean> all = repository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 2);
        KeyTableBean keyTableBean = all.get(0);
        KeyTableBean keyTableBean2 = all.get(1);
        log.info(JSON.toJSONString(all));
        Assert.assertTrue(11 == keyTableBean.getId());
        Assert.assertTrue(12 == keyTableBean2.getId());
    }

}
