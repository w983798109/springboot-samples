package dai.samples.jpa.repository.base;

import com.alibaba.fastjson.JSON;
import dai.samples.jpa.JpaApplication;
import dai.samples.jpa.entity.base.ColumnBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-29 14:53
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class ColumnBeanRepositoryTest {
    
    @Autowired
    private ColumnBeanRepository repository;

    @Before
    public void initTable() {
        repository.deleteAll();
    }
    
    @Test
    public void add() {
        ColumnBean bean = new ColumnBean();
        bean.setValue("value");
        bean.setNullValue(null);
        bean.setNum(12.3456D);
        bean.setTestBasic("懒加载");
        bean.setTestBasicV2("懒加载");
        bean.setNotInTable("not");
        bean.setCreateTime(new Date());
        bean.setColUnique("1");
        bean.setColUniqueV2("1");
        bean.setBigText("big");
        bean.setVersion(1L);
        repository.save(bean);
        List <ColumnBean> all = repository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
        log.info(JSON.toJSONString(all));
    }

}
