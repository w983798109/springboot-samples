package dai.samples.rabbit.base.manager.direct;

import dai.samples.rabbit.RabbitApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 *
 * @author daify
 * @date 2019-07-22 10:27
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RabbitApplication.class)
public class DirectSenderTest {
    
    @Autowired
    private DirectSender sender;

    @Test 
    public void send() {
        /*User user = new User("1","Direct",100);
        sender.send1(user);*/
    }
}
