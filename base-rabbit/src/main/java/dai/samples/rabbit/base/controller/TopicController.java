package dai.samples.rabbit.base.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.base.entity.User;
import dai.samples.rabbit.base.manager.topic.TopicSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author daify
 * @date 2019-07-22 15:31
 **/
@RestController
@RequestMapping("topic")
public class TopicController {

    @Autowired
    private TopicSender sender;

    @RequestMapping(value = "send",method = RequestMethod.GET)
    public String sendMessageV1() {
        User user = new User("5","Topic",500);
        sender.send(user);
        return JSON.toJSONString(user);
    }
}
