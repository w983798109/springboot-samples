package dai.samples.cache.entity;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author daify
 * @date 2019-07-17 10:19
 **/
@Data
public class User implements Serializable {

    private Long id;
    
    private String name;
    
    private String type;
}
