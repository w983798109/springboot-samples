package dai.samples.cache.dao;

import dai.samples.cache.entity.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * 此方法使用redis作为缓存
 * @author daify
 * @date 2020-10-13
 */
@Component
public class CacheDao {

    /**
     * 获取用户数据
     * @param user
     * @return
     */
    @Cacheable(value = "user",key = "#user.id")
    public User findUser(User user) {
        return new User();
    }

    /**
     * 设置用户数据
     * @param user
     * @return
     */
    @CachePut(value = "user",key = "#user.id")
    public User putUser(User user) {
        return user;
    }

    /**
     * 清空用户数据
     * @param user
     */
    @CacheEvict(value = "user",key = "#user.id")
    public void clear(User user) {

    }
}
