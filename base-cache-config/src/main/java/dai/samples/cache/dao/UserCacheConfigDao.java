package dai.samples.cache.dao;

import dai.samples.cache.entity.User;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * 正常时候缓存的时候需要指定cache的key和value参数，使用自定义的myCacheResolver可以在解析的时候完成相关内容
 * @author daify
 * @date 2020-10-17
 */
@CacheConfig(keyGenerator = "myKeyGenerator", cacheResolver = "myCacheResolver")
@Component
public class UserCacheConfigDao {

    /**
     * 获取用户数据
     * @param user
     * @return
     */
    @Cacheable()
    public User findUser(User user) {
        return new User();
    }

    /**
     * 设置用户数据
     * @param user
     * @return
     */
    @CachePut()
    public User putUser(User user) {
        return user;
    }

    /**
     * 清空用户数据
     * @param user
     */
    @CacheEvict()
    public void clear(User user) {

    }
}
