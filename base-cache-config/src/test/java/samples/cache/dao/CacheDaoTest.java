package samples.cache.dao;

import dai.samples.cache.CacheConfigApplication;
import dai.samples.cache.dao.CacheDao;
import dai.samples.cache.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * @author daify
 * @date 2020-10-17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CacheConfigApplication.class)
@EnableAutoConfiguration
@Slf4j
public class CacheDaoTest extends CacheConfigApplication {

    @Autowired
    private CacheDao cacheDao;

    /**
     * 获取数据
     * @return
     */
    @Test
    public void find() {
        User user = new User();
        user.setId(1L);
        user.setName("test");
        cacheDao.clear(user);
        User user1 = cacheDao.findUser(user);
        Assert.assertTrue(StringUtils.isEmpty(user1.getName()));
        // 设置数据
        cacheDao.putUser(user);
        // 查询数据
        User user2 = cacheDao.findUser(user);
        Assert.assertTrue("test".equals(user2.getName()));
        log.info("测试执行完毕");
    }
}
