package dai.samples.mybatis.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 *
 * @author daify
 * @date 2019-08-13 17:04
 **/
@Data
public class Page<T> {

    /**
     * 每页显示条数
     */
    private int pageSize;
    /**
     * 第几页
     */
    private int pageNum;
    /**
     * 总条数
     */
    private int pageTotal;

    private List<T> rows;
    
    public int getStart() {
        return (pageNum - 1) * pageSize;
    }

    public Page(int pageSize, int pageNum) {
        super();
        this.pageSize = pageSize;
        this.pageNum = pageNum;
    }

    public Page(Map <String, String> map){
        // 要查询的页数 
        if (map.get("pageNum") != null) {
            this.setPageNum(this.pageNum = Integer.parseInt(map.get("pageNum"))); 
        } else {
            // 设置初始值  
            this.setPageNum(1);
        }
        // 每页显示条数  
        if (map.get("numPerPage") != null) {
            this.setPageSize(Integer.parseInt(map.get("numPerPage")));
        } else {
            // 设置初始值  
            this.setPageSize(5);
        }

    }  
}
