package dai.samples.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

/**
 *
 * @author daify
 * @date 2019-08-01
 **/
@TableName
@Data
public class Order {

    @TableId(type = IdType.ID_WORKER)
    private String orderId;
    
    private String name;
    
    private String type;
    
    private Timestamp createTime;
    
    private Timestamp updateTime;
    
}
