package dai.samples.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dai.samples.mybatis.entity.Page;
import dai.samples.mybatis.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author daify
 * @date 2019-08-01
 **/
public interface UserMapper extends BaseMapper<User> {

    List<User> selectMyPage(@Param("page")Page<User> page, @Param("userAge") Integer userAge);
}
