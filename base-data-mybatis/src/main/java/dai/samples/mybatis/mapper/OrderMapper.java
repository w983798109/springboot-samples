package dai.samples.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dai.samples.mybatis.entity.Order;

/**
 *
 * @author daify
 * @date 2019-08-01
 **/
public interface OrderMapper extends BaseMapper<Order> {
}
