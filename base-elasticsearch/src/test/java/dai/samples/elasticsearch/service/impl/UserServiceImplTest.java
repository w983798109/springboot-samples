package dai.samples.elasticsearch.service.impl;

import com.alibaba.fastjson.JSON;
import dai.samples.elasticsearch.ElasticsearchApplication;
import dai.samples.elasticsearch.entity.User;
import dai.samples.elasticsearch.repository.UserRepository;
import dai.samples.elasticsearch.service.UserService;
import dai.samples.elasticsearch.service.mock.UserMock;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.metrics.sum.InternalSum;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;


/**
 * @author daify
 * @date 2019-08-03
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ElasticsearchApplication.class)
@Slf4j
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ElasticsearchTemplate template;

    @Before
    public void initData() {
        if (userService.indexExists(User.class)) {
            userRepository.deleteAll();
            template.deleteIndex(User.class);
        }
    }

    @Test
    public void findAllByScoreBetween() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        List<User> all = userService.findAllByScoreBetween(1, 3);
        log.info(JSON.toJSONString(all));
        Assert.assertTrue(3 == all.size());
        all.forEach(item -> Assert.assertTrue(item.getScore() <= 3 && item.getScore() >= 1));
    }

    /**
     * 测试索引的创建
     */
    @Test
    public void createIndex() {
        userService.createIndex(User.class);
        Assert.assertTrue(userService.indexExists(User.class));
    }

    /**
     * 测试索引的删除
     */
    @Test
    public void deleteIndex() {
        userService.createIndex(User.class);
        Assert.assertTrue(userService.indexExists(User.class));
        userService.deleteIndex(User.class);
        Assert.assertFalse(userService.indexExists(User.class));
    }

    @Test
    public void saveUser() {
        userService.createIndex(User.class);
        User one = UserMock.getOne();
        userService.saveUser(one);
        Optional<User> byId = userRepository.findById(one.getId());
        log.info(JSON.toJSONString(byId.get()));
        Assert.assertNotNull(byId);
        Assert.assertNotNull(byId.get());
        Assert.assertNotNull(one.getId().equals(byId.get().getId()));
    }

    @Test
    public void saveAllUser() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        Iterable<User> all = userRepository.findAll();
        all.forEach(item -> {
            log.info(JSON.toJSONString(item));
            Assert.assertNotNull(item);
        });
    }

    @Test
    public void findAll() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        list.sort((o1,o2) -> o2.getScore() - o1.getScore());
        log.info(JSON.toJSONString(list));
        userService.saveAllUser(list);
        List<User> score = userService.findAll("score");
        log.info(JSON.toJSONString(score));
        Assert.assertNotNull(score);
        Assert.assertTrue(score.size() == 5);
        Assert.assertTrue(score.get(0).getScore() < score.get(1).getScore());
    }

    @Test
    public void myQuery() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);

        Page<User> score = userService.myQuery("score", "3");
        Assert.assertTrue(1 == score.getContent().size());
        Assert.assertTrue(3 == score.getContent().get(0).getScore());

    }

    @Test
    public void myLikeQuery() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        Page<User> score = userService.myQuery("name", "用户");
        log.info(JSON.toJSONString(score.getContent()));
        Assert.assertTrue(5 == score.getContent().size());
    }

    @Test
    public void myLikeQuery2() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        Page<User> score = userService.myQuery("tag", "武汉美食");
        score.getContent().forEach(item->log.info("武汉美食查询出结果名称:"+ item.getTag()));
        log.info(JSON.toJSONString(score.getContent()));
        Assert.assertTrue(score.getContent().size() > 0);
    }

    @Test
    public void myPageQuery() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        Page<User> users = userService.myPageQuery("group", "一", 0, 10);
        Assert.assertTrue(2 < users.getContent().size());
        Page<User> users2 = userService.myPageQuery("group", "一", 0, 2);
        Assert.assertTrue(2 == users2.getContent().size());
    }

    @Test
    public void myQuerySort() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        Page<User> score = userService.myQuerySort("group", "一","score",SortOrder.ASC);
        List<User> content = score.getContent();
        Assert.assertTrue(content.size() > 1);
        Assert.assertTrue(content.get(0).getScore() < content.get(1).getScore());    }

    @Test
    public void aggStringTerms() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        List<StringTerms.Bucket> buckets = userService.aggStringTerms("groups", "group");
        buckets.forEach(bucket -> {
            log.info(bucket.getKeyAsString());
            log.info(String.valueOf(bucket.getDocCount()));
        });


    }

    @Test
    public void subAggStringTermsAvg() {
        userService.createIndex(User.class);
        List<User> list = UserMock.getList(5);
        userService.saveAllUser(list);
        List<StringTerms.Bucket> buckets =
                userService.subAggStringTermsSum("groups", "scoreSum", "group","score");
        buckets.forEach(bucket -> {
            log.info(bucket.getKeyAsString());
            log.info(String.valueOf(bucket.getDocCount()));
            InternalSum avg = (InternalSum) bucket.getAggregations().asMap().get("scoreSum");
            log.info(String.valueOf(avg));
        });
    }
}
