package dai.samples.elasticsearch.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 *
 * @author daify
 * @date 2019-07-17 15:44
 **/
@Document(indexName = "people", type = "user", shards = 1, replicas = 0)
@Data
public class User implements Serializable {

    private static final long serialVersionUID = -1L;

    /**
     * 城市编号
     */
    @Id
    private Long id;
    /**
     * 城市名称
     */
    @Field(type = FieldType.Text,
    analyzer = "analysis-ik")
    private String name;
    /**
     * 描述
     */
    @Field(type = FieldType.Text)
    private String description;
    /**
     * 评分
     */
    @Field(type = FieldType.Integer)
    private Integer score;

    /**
     * 分组内容
     */
    @Field(type = FieldType.Keyword)
    private String group;

    /**
     * 城市名称
     */
    @Field(type = FieldType.Text,
            analyzer = "analysis-ik")
    private String tag;
    
}
