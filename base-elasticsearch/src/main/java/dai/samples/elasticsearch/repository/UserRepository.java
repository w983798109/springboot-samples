package dai.samples.elasticsearch.repository;

import dai.samples.elasticsearch.entity.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-17 15:43
 **/
public interface UserRepository extends ElasticsearchRepository<User, Long>  {

    /**
     * 根据评分查询
     * @param score1
     * @param score2
     * @return
     */
    List<User> findAllByScoreBetween(Integer score1, Integer score2);
}
