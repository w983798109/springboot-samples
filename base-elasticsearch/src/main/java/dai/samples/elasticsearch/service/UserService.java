package dai.samples.elasticsearch.service;

import dai.samples.elasticsearch.entity.User;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-17 16:05
 **/
public interface UserService {

    /**
     * 获取评分区间值的内容
     * @param score1
     * @param score2
     * @return
     */
    List<User> findAllByScoreBetween(Integer score1, Integer score2);

    /**
     * 创建索引
     * @param clazz
     */
    void createIndex(Class clazz);

    /**
     * 删除
     * @param clazz
     */
    void deleteIndex(Class clazz);

    /**
     * 判断索引是否存在
     * @param clazz
     * @return
     */
    Boolean indexExists(Class clazz);

    /**
     * 保存用户
     * @param user
     * @return
     */
    Long saveUser(User user);

    /**
     * 批量保存用户
     * @param userList
     */
    void saveAllUser(List<User> userList);

    /**
     * 使用排序key进行排序获取所有数据
     * @param sortName
     * @return
     */
    List<User> findAll(String sortName);

    /**
     * 自定义的查询
     * @param key
     * @param value
     * @return
     */
    Page<User> myQuery(String key, String value);

    /**
     * 自定义的查询,模糊查询
     * @param key
     * @param value
     * @return
     */
    Page<User> myLikeQuery(String key, String value);

    /**
     * 自定义的分页查询
     * @param key
     * @param value
     * @param page
     * @param pageSize
     * @return
     */
    Page<User> myPageQuery(String key,String value,
                                  Integer page,Integer pageSize);

    /**
     * 排序查询
     * @param key
     * @param value
     * @param sortField
     * @param order
     * @return
     */
    Page<User> myQuerySort(String key, String value, String sortField, SortOrder order);

    /**
     * 聚合
     */
    List<StringTerms.Bucket> aggStringTerms(String termsName, String field);

    /**
     * 嵌套聚合
     */
    List<StringTerms.Bucket> subAggStringTermsSum(String termsName,String termsNameAvg,String field,String fieldAvg);
}
