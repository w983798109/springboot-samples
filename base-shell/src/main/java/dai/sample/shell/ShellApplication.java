package dai.sample.shell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author daify
 * @date 2021-03-22
 */
@SpringBootApplication
public class ShellApplication {

    public static void main(String[] args) {

        SpringApplication.run(ShellApplication.class,args);
    }

}
