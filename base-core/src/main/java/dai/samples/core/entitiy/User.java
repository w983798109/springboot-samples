package dai.samples.core.entitiy;

import lombok.Data;

/**
 *
 * @author daify
 * @date 2019-07-15 9:17
 **/
@Data
public class User {
    
    private Long id;
    
    private String name;
    
    private Integer age;
    
    private Double money;
}
