package dai.samples.jpa.repository.mock;

import dai.samples.jpa.entity.UserTag;

/**
 *
 * @author daify
 * @date 2019-08-21 17:39
 **/
public class UserTagMock {
    
    public static UserTag getDefTag1() {
        UserTag tag = new UserTag();
        tag.setTagName("test1");
        return tag;
    }

    public static UserTag getDefTag2() {
        UserTag tag = new UserTag();
        tag.setTagName("test2");
        return tag;
    }

    public static UserTag getDefTag3() {
        UserTag tag = new UserTag();
        tag.setTagName("test3");
        return tag;
    }
}
