package dai.samples.jpa.repository.mock;


import dai.samples.jpa.entity.UserCart;

import java.util.Date;

/**
 *
 * @author daify
 * @date 2019-07-30 20:43
 **/
public class UserCartMock {

    public static UserCart getDefUserCart1 () {
        UserCart userCart = new UserCart();
        userCart.setCount(10);
        userCart.setTotalMoney(888.8D);
        userCart.setUpdateTime(new Date());
        return userCart;
    }

    public static UserCart getDefUserCart2 () {
        UserCart userCart = new UserCart();
        userCart.setCount(20);
        userCart.setTotalMoney(888.8D);
        userCart.setUpdateTime(new Date());
        return userCart;
    }

    public static UserCart getDefUserCart3 () {
        UserCart userCart = new UserCart();
        userCart.setCount(30);
        userCart.setTotalMoney(888.8D);
        userCart.setUpdateTime(new Date());
        return userCart;
    }

    public static UserCart getDefUserCart4 () {
        UserCart userCart = new UserCart();
        userCart.setCount(40);
        userCart.setTotalMoney(888.8D);
        userCart.setUpdateTime(new Date());
        return userCart;
    }

    public static UserCart getDefUserCart5 () {
        UserCart userCart = new UserCart();
        userCart.setCount(50);
        userCart.setTotalMoney(888.8D);
        userCart.setUpdateTime(new Date());
        return userCart;
    }
}
