package dai.samples.jpa.repository;

import dai.samples.JpaApplication;
import com.alibaba.fastjson.JSON;
import dai.samples.jpa.entity.*;
import dai.samples.jpa.repository.mock.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author daify
 * @date 2019-07-30
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class CascadeTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserAddressRepository userAddressRepository;

    @Autowired
    private UserCartRepository userCartRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserTagRepository userTagRepository;
    
    @Autowired
    private OrderItemRepository orderItemRepository;
    
    @Before
    public void clearTable() {
        orderRepository.deleteAll();
        userAddressRepository.deleteAll();
        userCartRepository.deleteAll();
        orderRepository.deleteAll();
        orderItemRepository.deleteAll();
        userRepository.deleteAll();
        userRoleRepository.deleteAll();
        userTagRepository.deleteAll();
    }

    /**
     * 用户和用户购物车是一对一的关系
     * 用户全权限
     * 测试用户保存的时候同时保存购物车
     */
    @Test
    public void testUserSave() {
        User defUser1 = UserMock.getDefUser1();
        UserCart defUserCart1 = UserCartMock.getDefUserCart1();
        defUser1.setUserCart(defUserCart1);
        defUserCart1.setUser(defUser1);
        // 此时保存用户，用户购物车应该也被保存
        userRepository.save(defUser1);
        List <UserCart> all = userCartRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
    }

    /**
     * 测试删除用户购物车的时候，用户不会被删除
     */
    @Test
    public void testUserCartDelete() {
        User defUser1 = UserMock.getDefUser1();
        UserCart defUserCart1 = UserCartMock.getDefUserCart1();
        defUser1.setUserCart(defUserCart1);
        // 此时保存用户，用户购物车应该也被保存
        userRepository.save(defUser1);
        // 此时删除用户购物车并不会删除用户
        userCartRepository.deleteAll();
        List <User> all1 = userRepository.findAll();
        Assert.assertNotNull(all1);
        Assert.assertTrue(all1.size() == 1);
    }

    /**
     * 测试删除用户的时候，购物车会被删除
     */
    @Test
    public void testUserDelete() {
        User defUser1 = UserMock.getDefUser1();
        UserCart defUserCart1 = UserCartMock.getDefUserCart1();
        defUser1.setUserCart(defUserCart1);
        // 此时保存用户，用户购物车应该也被保存
        userRepository.save(defUser1);
        // 此时删除用户购物车并不会删除用户
        userRepository.delete(defUser1);
        List <UserCart> all = userCartRepository.findAll();
        Assert.assertTrue(all.size() == 0);
    }

    /**
     * 订单表和订单子项表一对多关系
     * 订单表全权限
     */
    @Test
    public void testOrder() {
        Order defOrder1 = OrderMock.getDefOrder1();
        List <OrderItem> allDefOrder = OrderItemMock.getAllDefOrder();
        // order 维持orderItem关系
        defOrder1.setOrderItems(allDefOrder);
        // orderItem维持order关系
        allDefOrder.forEach(o -> o.setOrder(defOrder1));
        Order save = orderRepository.save(defOrder1);
        List <OrderItem> all = orderItemRepository.findAll();
        Assert.assertTrue(all.size() == allDefOrder.size());

        orderItemRepository.delete(all.get(0));
        Order order = orderRepository.findById(save.getId()).get();
        Assert.assertNotNull(order);

        orderRepository.deleteById(order.getId());
        List <OrderItem> all1 = orderItemRepository.findAll();
        Assert.assertTrue(all1.size() == 0);

    }

    /**
     * 订单表和订单子项表一对多关系
     */
    @Test
    public void testUserTag() {
        User defUser1 = UserMock.getDefUser1();
        UserTag defTag1 = UserTagMock.getDefTag1();
        UserTag defTag2 = UserTagMock.getDefTag2();
        defUser1 = userRepository.save(defUser1);
        defTag1 = userTagRepository.save(defTag1);
        defTag2 = userTagRepository.save(defTag2);

        defUser1.getTags().add(defTag1);
        defUser1.getTags().add(defTag2);

        defUser1 = userRepository.save(defUser1);
        // 此时会报错，因为中间表关系为User维护
        try {
            userTagRepository.delete(defTag2);
        } catch (Exception e) {
            log.info(e.getCause().getMessage());
            Assert.assertTrue(e instanceof DataIntegrityViolationException);
        }
        // 修改User表中关系
        defUser1.setTags(new HashSet <>());
        defUser1.getTags().add(defTag1);
        defUser1 = userRepository.save(defUser1);
        // 此时可以删除
        userTagRepository.delete(defTag2);
        // 直接删除用户没问题
        userRepository.delete(defUser1);
    }
    
    /**
     * 测试级联保存和不级联保存
     * 由于User和UserTag进行了级联操作，所以保存User数据前无需保存UserTag
     * 由于User没有和UserRole进行级联操作，所以保存User数据前需要保存UserRole
     */
    @Test
    public void testSave() {
        User defUser1 = UserMock.getDefUser1();
        List <UserRole> allDefRole = UserRoleMock.getAllDefRole();
        userRoleRepository.saveAll(allDefRole);
        List <UserRole> roles = userRoleRepository.findAll();
        Assert.assertNotNull(roles);
        Assert.assertTrue(roles.size() == allDefRole.size());
        log.info("角色数据已经保存，数据为:{}" ,JSON.toJSONString(roles));
        
        List <UserTag> allDefTag = UserRoleMock.getAllDefTag();
        defUser1.setRoles(new HashSet <>(allDefRole));
        defUser1.setTags(new HashSet <>(allDefTag));
        userRepository.save(defUser1);

        List <UserTag> tags = userTagRepository.findAll();
        Assert.assertNotNull(tags);
        Assert.assertTrue(tags.size() == allDefTag.size());
        log.info("标签数据已经保存，数据为:{}" ,JSON.toJSONString(tags));

        List <User> all = userRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
        // 注意因为字段中使用了懒加载的原因使用JSON进行json的时候因为会尝试解析Roles和UserTag的内容，
        // 但是此时session已经关闭,会抛出org.hibernate.LazyInitializationException的异常
        // log.info("用户数据已经保存，数据为:{}" ,JSON.toJSONString(all));
    }

    /**
     * 测试级联的新增和删除
     * 订单和订单详情Order和OrderItem
     */
    @Test
    public void testCascadeTypeAll() {
        User defUser1 = UserMock.getDefUser1();
        User save = userRepository.save(defUser1);
        Order defOrder1 = OrderMock.getDefOrder1();
        List <OrderItem> allDefOrder = OrderItemMock.getAllDefOrder();
        defOrder1.setOrderItems(allDefOrder);
        defOrder1.setUser(save);
        Order save1 = orderRepository.save(defOrder1);
        List <Order> orders = orderRepository.findAll();
        Assert.assertNotNull(orders);
        Assert.assertTrue(orders.size() == 1);

        List <OrderItem> orderItems = orderItemRepository.findAll();
        Assert.assertNotNull(orderItems);
        Assert.assertTrue(orderItems.size() == allDefOrder.size());
        
        orderRepository.deleteById(save1.getId());

        List <Order> ordersDelete = orderRepository.findAll();
        Assert.assertTrue(CollectionUtils.isEmpty(ordersDelete));

        List <OrderItem> orderItemsDelete = orderItemRepository.findAll();
        Assert.assertTrue(CollectionUtils.isEmpty(orderItemsDelete));
    }

    /**
     * 测试级联的修改
     * 订单和订单详情Order和OrderItem
     */
    @Test
    public void testCascadeRefresh() {
        User defUser1 = UserMock.getDefUser1();
        List <UserRole> allDefRole = UserRoleMock.getAllDefRole();
        userRoleRepository.saveAll(allDefRole);
        defUser1.setRoles(new HashSet <>(allDefRole));
        userRepository.save(defUser1);

        List <UserRole> userRoles = userRoleRepository.findAll();
        Assert.assertNotNull(userRoles);
        userRoles.forEach(item -> {
            Assert.assertTrue(!"修改后的名字".equals(item.getRoleName()));
        });

        User one = userRepository.findById(defUser1.getId()).get();
        Set <UserRole> roles = one.getRoles();
        roles.forEach(item -> item.setRoleName("修改后的名字"));
        one.setRoles(roles);
        userRepository.save(one);
        
        List <UserRole> userRolesNew = userRoleRepository.findAll();
        Assert.assertNotNull(userRolesNew);
        userRolesNew.forEach(item -> {
            Assert.assertTrue("修改后的名字".equals(item.getRoleName()));
        });
    }
    
    
    
    
/*    ### 需要测试内容

- 一对一级联更新
- 一对多级联更新
- 多对一级联更新
- 多对多级联更新

- 一对一级联删除
- 一对一级联不删除

- 一对多级联删除
- 一对多级联不级联删除*/
}
