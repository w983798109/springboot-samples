package dai.samples.jpa.repository.mock;

import dai.samples.jpa.entity.UserCartItem;

/**
 *
 * @author daify
 * @date 2019-07-30 20:45
 **/
public class UserCartItemMock {


    public UserCartItem getDefCartItem1() {
        UserCartItem cartItem = new UserCartItem();
        cartItem.setMoney(10D);
        cartItem.setProductId("SKU111");
        return cartItem;
    }

    public UserCartItem getDefCartItem2() {
        UserCartItem cartItem = new UserCartItem();
        cartItem.setMoney(20D);
        cartItem.setProductId("SKU222");
        return cartItem;
    }

    public UserCartItem getDefCartItem3() {
        UserCartItem cartItem = new UserCartItem();
        cartItem.setMoney(30D);
        cartItem.setProductId("SKU333");
        return cartItem;
    }

    public UserCartItem getDefCartItem4() {
        UserCartItem cartItem = new UserCartItem();
        cartItem.setMoney(40D);
        cartItem.setProductId("SKU444");
        return cartItem;
    }

    public UserCartItem getDefCartItem5() {
        UserCartItem cartItem = new UserCartItem();
        cartItem.setMoney(50D);
        cartItem.setProductId("SKU555");
        return cartItem;
    }

    public UserCartItem getDefCartItem6() {
        UserCartItem cartItem = new UserCartItem();
        cartItem.setMoney(60D);
        cartItem.setProductId("SKU666");
        return cartItem;
    }

}
