package dai.samples.jpa.repository;

import dai.samples.JpaApplication;
import dai.samples.jpa.entity.Order;
import dai.samples.jpa.repository.mock.OrderMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author daify
 * @date 2019-07-31
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class JpaHqlOrderTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserAddressRepository userAddressRepository;

    @Autowired
    private UserCartRepository userCartRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserTagRepository userTagRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Before
    public void clearTable() {
        orderRepository.deleteAll();
        userAddressRepository.deleteAll();
        userCartRepository.deleteAll();
        userRepository.deleteAll();
        userRoleRepository.deleteAll();
        userTagRepository.deleteAll();
    }

    @Test
    public void testQueryPage() {
        orderRepository.save(OrderMock.getDefOrder1());
        orderRepository.save(OrderMock.getDefOrder1());
        orderRepository.save(OrderMock.getDefOrder1());
        Pageable pageable = PageRequest.of(0,2);
        Page <Order> all = orderRepository.findAll(pageable);
        Assert.assertNotNull(all);
        Assert.assertTrue(all.getContent().size() == 2);
    }

}
