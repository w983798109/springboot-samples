package dai.samples.jpa.service;

import dai.samples.JpaApplication;
import dai.samples.jpa.bean.UserCartInfo;
import dai.samples.jpa.entity.User;
import dai.samples.jpa.entity.UserCart;
import dai.samples.jpa.repository.UserCartRepository;
import dai.samples.jpa.repository.UserRepository;
import dai.samples.jpa.repository.mock.UserCartMock;
import dai.samples.jpa.repository.mock.UserMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author daify
 * @date 2019-07-31
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCartRepository userCartRepository;
    

    @Before
    public void clearTable() {
        userCartRepository.deleteAll();
        userRepository.deleteAll();
    }
    
    @Autowired
    private UserService userService;

    @Test 
    public void getUserWithNamedQuery() {
        User defUser1 = UserMock.getDefUser1();
        userRepository.save(defUser1);
        User user = userService.getUserWithNativeQuery(defUser1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getName(),defUser1.getName());

    }

    @Test 
    public void getUserWithQuery() {
        User defUser1 = UserMock.getDefUser1();
        userRepository.save(defUser1);
        User user = userService.getUserWithQuery(defUser1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getName(),defUser1.getName());
    }

    @Test
    public void getUserInfoWithNativeQuery() {
        User defUser1 = UserMock.getDefUser1();
        UserCart defUserCart1 = UserCartMock.getDefUserCart1();
        userRepository.save(defUser1);
        defUserCart1.setUser(defUser1);
        userCartRepository.save(defUserCart1);
        UserCartInfo info = userService
                .getUserInfoWithNativeQuery(defUser1.getId());
        Assert.assertNotNull(info);
        Assert.assertEquals(info.getName(),defUser1.getName());
        Assert.assertEquals(info.getTotalMoney(),defUserCart1.getTotalMoney());

    }

    @Test 
    public void getUserWithBuilder() {
        User defUser1 = UserMock.getDefUser1();
        userRepository.save(defUser1);
        User queryUser = new User();
        queryUser.setName(defUser1.getName());
        User user = userService.getUserWithBuilder(queryUser);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getName(),defUser1.getName());
    }
}