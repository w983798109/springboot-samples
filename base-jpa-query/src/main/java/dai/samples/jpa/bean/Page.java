package dai.samples.jpa.bean;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 *
 * @author daify
 * @date 2019-07-31
 **/
public class Page implements Pageable {
    /**
     * Returns the page to be returned.
     *
     * @return the page to be returned.
     */
    @Override 
    public int getPageNumber() {
        return 0;
    }

    /**
     * Returns the number of items to be returned.
     *
     * @return the number of items of that page
     */
    @Override 
    public int getPageSize() {
        return 0;
    }

    /**
     * Returns the offset to be taken according to the underlying page and page size.
     *
     * @return the offset to be taken
     */
    @Override 
    public long getOffset() {
        return 0;
    }

    /**
     * Returns the sorting parameters.
     *
     * @return
     */
    @Override public Sort getSort() {
        return null;
    }

    /**
     * Returns the {@link Pageable} requesting the next {@link Page}.
     *
     * @return
     */
    @Override public Pageable next() {
        return null;
    }

    /**
     * Returns the previous {@link Pageable} or the first {@link Pageable} if the current one already is the first one.
     *
     * @return
     */
    @Override public Pageable previousOrFirst() {
        return null;
    }

    /**
     * Returns the {@link Pageable} requesting the first page.
     *
     * @return
     */
    @Override public Pageable first() {
        return null;
    }

    /**
     * Returns whether there's a previous {@link Pageable} we can access from the current one. Will return
     * {@literal false} in case the current {@link Pageable} already refers to the first page.
     *
     * @return
     */
    @Override public boolean hasPrevious() {
        return false;
    }
}
