package dai.samples.jpa.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author daify
 * @date 2019-07-31
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCartInfo {
    
    private BigInteger id;

    private String name;

    private Integer age;

    private Double point;

    private BigInteger cartId;

    private Integer count;

    private Double totalMoney;

    private Date updateTime;
}
