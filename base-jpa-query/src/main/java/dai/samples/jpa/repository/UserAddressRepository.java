package dai.samples.jpa.repository;

import dai.samples.jpa.entity.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-30 16:32
 **/
public interface UserAddressRepository
        extends JpaRepository <UserAddress, Long> {
}