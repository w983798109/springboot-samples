package dai.samples.jpa.repository;

import dai.samples.jpa.entity.UserCart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-30 16:32
 **/
public interface UserCartRepository
        extends JpaRepository <UserCart, Long> {
}