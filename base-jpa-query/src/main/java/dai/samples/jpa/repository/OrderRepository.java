package dai.samples.jpa.repository;

import dai.samples.jpa.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author daify
 * @date 2019-07-30 16:32
 **/
public interface OrderRepository
        extends JpaRepository <Order, Long> {

    @Query(value = "from Order where id =?1")
    Order selecOrderByIde(@Param("id") Long id);
}