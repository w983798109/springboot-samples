package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 用户标签
 * @author daify
 **/
@Data
@Entity
@Table(name = "cascade_user_tag")
public class UserTag {

    @Id
    @GeneratedValue
    private Long id;
    
    private String tagName;

    @ManyToMany(mappedBy = "tags",
            cascade = {},
            fetch = FetchType.LAZY)
    private List<User> userList;
}
