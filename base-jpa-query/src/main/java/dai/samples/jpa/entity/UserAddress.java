package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 用户地址
 * @author daify
 * @date 2019-07-30 9:53
 **/
@Data
@Entity
@Table(name = "cascade_user_address")
public class UserAddress {

    @Id
    @GeneratedValue
    private Long id;
    
    private String name;
    
    private String tel;

    private String address;
    
    @ManyToOne(
            targetEntity = User.class,
            cascade = {},
            fetch = FetchType.EAGER)
    @JoinColumn(
            // 设置在OrderItem表中的关联字段(外键)
            name="user_id"
    )
    private User user;
}
