package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户角色
 * @author daify
 * @date 2019-07-30 9:52
 **/
@Data
@Entity
@Table(name = "cascade_user_role")
@NamedQuery(
        name = "UserRole.selectRoleByName",
        query = "select ur from UserRole ur where ur.roleName = :roleName"
)
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "UserRole.selectRoleByNameWithSql",
                query = "select * from cascade_user_role ur " 
                        + "where ur.role_name = :roleName",
                resultClass = UserRole.class
        ),
        @NamedNativeQuery(
                name = "UserRole.selectRoleByIdWithSql",
                query = "select *,'auth' as auth_name from cascade_user_role ur "
                        + "where ur.id = :id",
                resultSetMapping = "userRoleMapping"
        )
})
/**自定义的返回集合**/
@SqlResultSetMapping(name = "userRoleMapping",
        entities = {
            @EntityResult(
                    entityClass = UserRole.class,
                    fields = {
                            @FieldResult(name="id",column="id"),
                            @FieldResult(name="roleName", column="auth_name"),
                            @FieldResult(name="auth",column="auth")  
                    }
            )
        })
public class UserRole implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    private String roleName;

    private String auth;

}
