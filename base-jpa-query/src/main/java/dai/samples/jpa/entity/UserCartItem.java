package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;

/**
 *
 * @author daify
 * @date 2019-07-30
 **/
@Data
@Entity
@Table(name = "cascade_user_cart_item")
public class UserCartItem {

    @Id
    @GeneratedValue
    private Long id;
    
    private String productId;
    
    private Double money;

    @ManyToOne(targetEntity = UserCart.class,
            cascade = {},
            fetch = FetchType.EAGER)
    private UserCart userCart;


    @Override
    public String toString() {
        return "UserCartItem{" +
                "id=" + id +
                ", productId='" + productId + '\'' +
                ", money=" + money +
                ", userCart=" + userCart +
                '}';
    }
}
