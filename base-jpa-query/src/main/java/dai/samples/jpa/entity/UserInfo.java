package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;

/**
 *
 * @author daify
 * @date 2019-08-21 14:18
 **/
@Data
@Entity
@Table(name = "cascade_user_info")
public class UserInfo {

    @Id
    @GeneratedValue
    private Long id;
    
    private String userInfo;
    
    private String config;

}
