package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 订单
 * @author daify
 **/
@Data
@Entity
@Table(name = "cascade_order")
public class Order {

    @Id
    @GeneratedValue
    private Long id;
    
    private String orderNo;
    
    private Integer count;
    
    private Double money;
    
    @ManyToOne(targetEntity = User.class,
                cascade = {})
    private User user;

    @OneToMany(mappedBy = "order",
            targetEntity = OrderItem.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private List<OrderItem> orderItems;
    
}