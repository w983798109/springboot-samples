package dai.samples.jpa.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 订单子项
 * @author daify
 **/
@Data
@Entity
@Table(name = "cascade_order_item")
public class OrderItem {

    @Id
    @GeneratedValue
    private Long id;

    private String productId;

    private Double money;
    
    private Integer count;
    
    private String productName;

    @ManyToOne(targetEntity = Order.class,
            cascade = {},
            fetch = FetchType.EAGER)
    @JoinColumn(
            // 设置在OrderItem表中的关联字段(外键)
            name="order_id"
    )
    // 防止json序列化死循环问题解决
    @JsonBackReference
    private Order order;

    @Override 
    public String toString() {
        return "OrderItem{" + "id=" + id + ", productId='" + productId + '\'' + ", money=" + money
               + ", count=" + count + ", productName='" + productName + '\'' + ", order=" + order
               + '}';
    }
}
