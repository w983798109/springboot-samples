package dai.samples.rabbit.converter.config;


import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;

/**
 * @author daify
 * @date 2019-07-22
 */
@Slf4j
public class MyMessageConverter implements MessageConverter {

    /**
     * 将java对象和属性对象转换成Message对象
     * @param o
     * @param messageProperties
     * @return
     * @throws MessageConversionException
     */
    @Override
    public Message toMessage(Object o, MessageProperties messageProperties) throws MessageConversionException {
        log.info("=======toMessage=========");
        String s = null;
        if (o instanceof User) {
            s = JSON.toJSONString(o);
        } else {
            s = o.toString();
        }
        return new Message(s.getBytes(),messageProperties);
    }

    /**
     * 将消息对象转换成java对象。
     * @param message
     * @return
     * @throws MessageConversionException
     */
    @Override
    public Object fromMessage(Message message) throws MessageConversionException {
        log.info("=======fromMessage=========");
        String content = null;
        try {
            content = new String(message.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (content == null) {
            //都没有符合，则转换成字节数组
            return message.getBody();
        } else {
            return content;
        }
    }
}
