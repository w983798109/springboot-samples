package dai.samples.rabbit.errhandler.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.RabbitListenerErrorHandler;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author daify
 * @date 2019-07-23 10:05
 **/
@Slf4j
@Component(value = "myReceiverListenerErrorHandler")
public class MyReceiverListenerErrorHandler implements RabbitListenerErrorHandler {
    
    private static ConcurrentSkipListMap<Object, AtomicInteger> map = new ConcurrentSkipListMap();
    
    /**
     */
    @Override 
    public Object handleError(Message amqpMessage,
                                        org.springframework.messaging.Message <?> message,
                                        ListenerExecutionFailedException exception)
            throws Exception {
        log.error("消息接收发生了错误，消息内容:{},错误信息:{}",
                JSON.toJSONString(message.getPayload()), 
                JSON.toJSONString(exception.getCause().getMessage()));
        throw new AmqpRejectAndDontRequeueException("超出次数");
    }
}
