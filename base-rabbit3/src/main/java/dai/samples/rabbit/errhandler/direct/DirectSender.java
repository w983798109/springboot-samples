package dai.samples.rabbit.errhandler.direct;

import dai.samples.rabbit.entity.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:51
 **/

@Component
public class DirectSender {
    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send1(User user) {
        this.rabbitTemplate.convertAndSend(
                DirectConfig.DIRECT_EXCHANGE, 
                // routingKey
                "direct.V1", 
                user);
    }

    public void send2(User user) {
        this.rabbitTemplate.convertAndSend(
                DirectConfig.DIRECT_EXCHANGE,
                // routingKey
                "direct.V2",
                user);
    }
}
