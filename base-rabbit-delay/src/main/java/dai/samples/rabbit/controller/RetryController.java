package dai.samples.rabbit.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.entity.User;
import dai.samples.rabbit.retry.RetrySender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author daify
 * @date 2019-07-24 11:53
 **/
@RestController
@RequestMapping("retry")
public class RetryController {

    @Autowired
    private RetrySender sender;

    /**
     * 测试重试3次后完成处理
     * @return
     */
    @RequestMapping(value = "send",method = RequestMethod.GET)
    public String sendMessage() {
        User user = new User("3","Direct3",200,0);
        sender.send(user);
        return JSON.toJSONString(user);
    }

    /**
     * 测试重试3次后转入失败队列中
     * @return
     */
    @RequestMapping(value = "send2",method = RequestMethod.GET)
    public String sendMessage2() {
        User user = new User("1","Direct1",200,0);
        sender.send(user);
        return JSON.toJSONString(user);
    }
}
