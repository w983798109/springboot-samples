package dai.samples.rabbit.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.delay.DelaySender;
import dai.samples.rabbit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author daify
 * @date 2019-07-24 11:53
 **/
@RestController
@RequestMapping("delay")
public class DelayController {

    @Autowired
    private DelaySender sender;

    /**
     * 发送一条消息，此消息延迟时间为4秒
     * @return
     */
    @RequestMapping(value = "send",method = RequestMethod.GET)
    public String sendMessage() {
        User user = new User("1","Direct",100,0);
        sender.send(user);
        return JSON.toJSONString(user);
    }

    @RequestMapping(value = "send/{time}",method = RequestMethod.GET)
    public String sendMessage(@PathVariable("time")long time) {
        User user = new User("2","Direct2",200,0);
        sender.send2(user,time);
        return JSON.toJSONString(user);
    }
}
