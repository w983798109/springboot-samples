package dai.samples.rabbit.retry;

import dai.samples.rabbit.entity.User;
import dai.samples.rabbit.retry.config.RetryConfig;
import dai.samples.rabbit.retry.config.WorkConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:51
 **/

@Component
public class RetrySender {
    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(User user) {
        this.rabbitTemplate.convertAndSend(
                WorkConfig.WORK_EXCHANGE,
                // routingKey
                WorkConfig.WORK_KEY,
                user);
    }
}
