package dai.samples.rabbit.delay;

import dai.samples.rabbit.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:51
 **/

@Component
@Slf4j
public class DelaySender {
    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(User user) {
        log.info("消息已经发送，时间为：{}",
                new Timestamp(System.currentTimeMillis()));
        this.rabbitTemplate.convertAndSend(
                DelayConfig.DELAY_EXCHANGE,
                // routingKey
                DelayConfig.DELAY_QUEUE,
                user);
    }

    public void send2(User user,Long time) {
        log.info("消息已经发送，时间为：{}",new Timestamp(System.currentTimeMillis()));
        this.rabbitTemplate.convertAndSend(
                DelayConfig.DELAY_EXCHANGE,
                // routingKey
                DelayConfig.DELAY_QUEUE,
                user,
                message -> {
                    // 设置延迟毫秒值
                    message.getMessageProperties().setExpiration(String.valueOf(time));
                    return message;
                });
    }
}
