package dai.samples.shiro.service;

import dai.samples.shiro.entity.User;
import dai.samples.shiro.vo.UserVo;

/**
 * 用户管理接口
 */
public interface UserService {

    /**
     * 根据登录名获取用户信息
     * @param loginName
     * @return
     */
    UserVo getUserByName(String loginName);
}
