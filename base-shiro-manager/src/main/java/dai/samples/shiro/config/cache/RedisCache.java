package dai.samples.shiro.config.cache;


import lombok.extern.log4j.Log4j2;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.session.Session;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

/**
 * 会话缓存  使用redis作为缓存
 * @author Administrator
 */
@Log4j2
@Component
public class RedisCache implements Cache<String,Session> {

    @Autowired
    RedisTemplate<String, Session> redisTemplate;

    private String key = "SessionCache";

    public RedisCache() {
    }

    private BoundHashOperations<String, String, Session> getCache() {
        BoundHashOperations<String, String, Session> hashOps = redisTemplate.boundHashOps(key);
        return hashOps;
    }

    @Override
    public Session get(String o) throws CacheException {
        log.info("SessionCache get:{}",o);
        Session session = getCache().get(o);
        return session;
    }


    @Override
    public Session put(String s, Session session) throws CacheException {
        log.info("SessionCache put:{},{}",s,session);
        getCache().put(s,session);
        return session;
    }

    @Override
    public Session remove(String s) throws CacheException {
        log.info("SessionCache remove:{}",s);
        Session session = get(s);
        if (session != null) {
            getCache().delete(s);
        }
        return session;
    }

    @Override
    public void clear() throws CacheException {
        log.info("SessionCache clear");
        redisTemplate.delete(key);
    }

    @Override
    public int size() {
        log.info("SessionCache size");
        Long size = getCache().size();
        return size.intValue();
    }

    @Override
    public Set keys() {
        log.info("SessionCache keys");
        return getCache().keys();
    }

    @Override
    public Collection values() {
        log.info("SessionCache values");

        return getCache().values();
    }
}
