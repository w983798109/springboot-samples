package dai.samples.shiro.config.cache;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * redis的配置，主要是配置序列化工具
 * @author daify
 * @date 2019-07-16
 */
@Configuration
public class RedisConfig {

    /**
     * 配置redis 连接
     */
    @Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate template = new RedisTemplate<>();
        // json序列化
        template.setConnectionFactory(factory);
        // 3.创建 自定义序列化类
        MyRedisSerializer myRedisSerializer = new MyRedisSerializer();
        // 7.设置 value 的转化格式和 key 的转化格式 默认使用的是JdkSerializationRedisSerializer
        template.setValueSerializer(myRedisSerializer);
        template.setHashValueSerializer(myRedisSerializer);
        // 设置键（key）的序列化采用StringRedisSerializer。
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setDefaultSerializer(myRedisSerializer);

        return template;
    }
}
