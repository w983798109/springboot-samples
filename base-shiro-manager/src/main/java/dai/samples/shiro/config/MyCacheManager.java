package dai.samples.shiro.config;

import dai.samples.shiro.config.cache.MapCache;
import dai.samples.shiro.config.cache.RedisCache;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 缓存管理器
 * 用来管理不同业务模块使用的缓存
 */
@Component
public class MyCacheManager implements CacheManager {

    @Autowired
    private RedisCache cache;


    /**
     * 获得缓存
     * @param s 这里可以根据S拿到不同的缓存
     * @param <K>
     * @param <V>
     * @return
     * @throws CacheException
     */
    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        // 此时保存的是user_realm的缓存
        if (Constants.USER_REALM.equals(s)) {
            return new MapCache();
        } else {// 此时返回的是 session 会话中的缓存
            return (Cache<K, V>) cache;

        }
    }
}
