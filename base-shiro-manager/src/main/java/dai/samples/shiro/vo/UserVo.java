package dai.samples.shiro.vo;

import dai.samples.shiro.entity.User;
import lombok.Data;

import java.util.List;

/**
 * 用户数据视图
 */
@Data
public class UserVo extends User {

    /**
     * 角色数据视图
     */
    private List<RoleVo> roleVos;
}
