package dai.samples.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户ID和角色ID关系
 */
@Data
@TableName
public class UserRole {

    /**
     * 记录ID
     */
    @TableId
    private long urId;

    /**
     * 用户ID
     */
    private long userId;

    /**
     * 角色ID
     */
    private long roleId;

}
