package dai.samples.shiro.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;
import java.util.Map;

/**
 *
 * 自定义缓存
 * @author Administrator
 */
@Data
@TableName
@JsonIgnoreProperties(ignoreUnknown = true)
public class MySession {

    /**
     * 本地session信息
     */
    @TableId
    private String id;

    /**
     * 开始时间
     */
    private Date startTimestamp;

    /**
     * 最后一次时间
     */
    private Date lastAccessTime;

    /**
     * 过期时间
     */
    private long timeout;

    /**
     * 主机地址
     */
    private String host;

    /**
     * 属性JSON
     */
    private String attributeJson;

    private transient boolean expired;

    @TableField(exist = false)
    private Map<Object, Object> attributes;

    @TableField(exist = false)
    private transient Date stopTimestamp;

    public Map<Object, Object>  getAttributes() {
        if (attributes == null) {
            attributes = JSON.parseObject(attributeJson,Map.class);
        }
        return attributes;
    }

    public String getAttributeJson() {
        attributeJson = JSON.toJSONString(attributes);
        return attributeJson;
    }
}
