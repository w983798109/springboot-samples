package dai.samples.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色
 */
@Data
@TableName
public class Role implements Serializable {
    /**
     * 角色Id
     */
    @TableId
    private long roleId;
    /**
     * 角色名称
     */
    private String roleName;


}
