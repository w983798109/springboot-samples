package dai.samples.shiro.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "remember")
public class RememberController {

    /**
     * 查看remember信息
     * @return
     */
    @RequestMapping(value = "info")
    public String getInfo() {
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        boolean remembered = subject.isRemembered();
        String message = authenticated ? "已登录" : "未登录";
        message = message + "," + (remembered ? "已记住" : "未记住");
        return message;
    }
}
