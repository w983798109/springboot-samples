package dai.samples.log.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author daify
 * @date 2019-07-18
 */
@Slf4j
@Service
public class LogService {

    public String infoTest() {
        String str = "info";
        log.info("INFO的日志输出：{}",str);
        return str;
    }

    public String warnTest() {
        String str = "warn";
        log.warn("INFO的日志输出：{}",str);
        return str;
    }

    public String errorTest() {
        String str = "error";
        log.error("INFO的日志输出：{}",str);
        return str;
    }

    public String debugTest() {
        String str = "debug";
        log.debug("INFO的日志输出：{}",str);
        return str;
    }

    public String traceTest() {
        String str = "trace";
        log.trace("INFO的日志输出：{}",str);
        return str;
    }

}
